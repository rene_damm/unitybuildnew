﻿namespace UnityBuild.ConsoleRunner
{
	public class UnityBuildConsoleRunner
		: UnityBuildRunner
	{
		#region Public Methods

		public static int Main( string[] args )
		{
			return new UnityBuildConsoleRunner().Run( args );
		}

		#endregion
	}
}
