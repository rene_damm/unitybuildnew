﻿using NUnit.Framework;

namespace UnityBuild.Tests.Helpers
{
	[ Category( "UnitTests" ) ]
	public abstract class UnitTestFixture
		: BaseTestFixture
	{
	}
}
