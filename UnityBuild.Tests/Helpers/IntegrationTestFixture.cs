﻿using NUnit.Framework;

namespace UnityBuild.Tests.Helpers
{
	[ Category( "IntegrationTests" ) ]
	public abstract class IntegrationTestFixture
		: BaseTestFixture
	{
	}
}