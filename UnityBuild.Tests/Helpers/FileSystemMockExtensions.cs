﻿using System;
using Telerik.JustMock;
using UnityBuild.Files;

namespace UnityBuild.Tests.Helpers
{
    public static class FileSystemMockExtensions
    {
        #region Public Methods

		public static IFileSystem HasAnyFile( this IFileSystem fileSystem )
		{
			Mock.Arrange( () => fileSystem.GetFileInfo( Arg.AnyString ) )
				.Returns( ( string path ) =>
					BaseTestFixture.NewMock< IFileInfo >()
						.WithPath( path )
						.WithLastWriteTime( new DateTime( 1987, 9, 1 ) ) );
			return fileSystem;
		}

        public static IFileInfo HasFile( this IFileSystem fileSystem, string path )
        {
            var info = BaseTestFixture.NewMock< IFileInfo >()
                .WithPath( path )
                .WithLastWriteTime( DateTime.Now ); // Give it a default write time.

            Mock.Arrange( () => fileSystem.GetFileInfo( path ) )
				.Returns( info )
				.MustBeCalled();

            return info;
        }

		public static IFileSystem HasFileListing( this IFileSystem fileSystem, string path, params string[] results )
		{
			Mock.Arrange( () => fileSystem.ListFiles( path ) )
				.Returns( results )
				.MustBeCalled();
			return fileSystem;
		}

        #endregion
    }
}