﻿using Telerik.JustMock;
using UnityBuild.Tools;

namespace UnityBuild.Tests.Helpers
{
	public static class ToolInputsMockExtensions
	{
		#region Public Methods

		public static IToolInputs WithSettings( this IToolInputs inputs, IToolSettings settings )
		{
			Mock.Arrange( () => inputs.ToolSettings ).Returns( settings );
			return inputs;
		}

		#endregion
	}
}