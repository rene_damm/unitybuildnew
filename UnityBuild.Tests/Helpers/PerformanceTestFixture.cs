﻿using NUnit.Framework;

namespace UnityBuild.Tests.Helpers
{
	[ Category( "PerformanceTests" ) ]
	public abstract class PerformanceTestFixture
		: BaseTestFixture
	{
	}
}