﻿using System;
using Telerik.JustMock;
using UnityBuild.Files;

namespace UnityBuild.Tests.Helpers
{
	public static class FileInfoMockExtensions
	{
		#region Public Methods

		public static IFileInfo WithPath( this IFileInfo info, string path )
		{
			Mock.Arrange( () => info.Path ).Returns( path );
			return info;
		}

        public static IFileInfo WithLastWriteTime( this IFileInfo info, DateTime time )
        {
            Mock.Arrange( () => info.LastWriteTime ).Returns( time );
            return info;
        }

		#endregion
	}
}