﻿using Telerik.JustMock;
using UnityBuild.Utilities;

namespace UnityBuild.Tests.Helpers
{
	public static class NamedMockExtensions
	{
		#region Public Methods

		public static T WithName< T >( this T named, string name )
			where T : INamed
		{
			Mock.Arrange( () => named.Name ).Returns( name );
			return named;
		}

		#endregion
	}
}