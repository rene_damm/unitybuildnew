﻿using Telerik.JustMock;
using UnityBuild.Utilities;

namespace UnityBuild.Tests.Helpers
{
	public static class HashedMockExtensions
	{
		#region Public Methods

		public static T WithHash< T >( this T hashed, IHash hash )
			where T : IHashed
		{
			Mock.Arrange( () => hashed.Hash ).Returns( hash );
			return hashed;
		}

		#endregion
	}
}