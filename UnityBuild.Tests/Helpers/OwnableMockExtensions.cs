﻿using Telerik.JustMock;
using UnityBuild.Utilities;

namespace UnityBuild.Tests.Helpers
{
	public static class OwnableMockExtensions
	{
		#region Public Methods

		public static TOwnable OwnableByAny< TOwner, TOwnable >( this TOwnable ownable )
			where TOwnable : IOwnableBy< TOwner >
		{
			Mock.ArrangeSet( () => ownable.Owner = Arg.IsAny< TOwner >() );
			return ownable;
		}

		public static TOwnable OwnableOnlyBy< TOwner, TOwnable >( this TOwnable ownable, TOwner owner )
			where TOwnable : IOwnableBy< TOwner >
		{
			Mock.ArrangeSet( () => ownable.Owner = Arg.IsAny< TOwner >() )
				.OccursNever();
			Mock.ArrangeSet( () => ownable.Owner = owner )
				.MustBeCalled();

			return ownable;
		}

		#endregion
	}
}