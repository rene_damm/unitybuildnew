﻿using System.Linq;
using Telerik.JustMock;
using UnityBuild.Builds;

namespace UnityBuild.Tests.Helpers
{
	/// <summary>
	/// Extension methods for <see cref="IBuildInputs"/> to simplify mocking instances.
	/// </summary>
	public static class BuildInputsMockExtensions
	{
		#region Public Methods

		public static IBuildInputs WithBuildSteps( this IBuildInputs inputs, params IBuildStep[] steps )
		{
			Mock.Arrange( () => inputs.BuildSteps ).Returns( steps.ToList() );
			return inputs;
		}

		public static IBuildInputs WithoutBuildSteps( this IBuildInputs inputs )
		{
			Mock.Arrange( () => inputs.BuildSteps ).Returns( Enumerable.Empty< IBuildStep >().ToList() );
			return inputs;
		}

		#endregion
	}
}