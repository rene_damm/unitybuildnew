﻿using Telerik.JustMock;
using UnityBuild.Targets;
using UnityBuild.Tools;

namespace UnityBuild.Tests.Helpers
{
	public static class TargetCollectionMockExtensions
	{
		#region Public Methods

		public static ITargetCollection CreatesAnyTarget( this ITargetCollection targetCollection )
		{
			Mock.Arrange( () => targetCollection.FindOrAddTargetByName( Arg.AnyString, Arg.IsAny< ITargetSettings >() ) )
				.Returns( ( string name, ITargetSettings settings ) =>
					BaseTestFixture.NewTarget()
						.WithName( name )
						.WithSettings( settings )
						.WithoutBuildSteps() );

			return targetCollection;			
		}
		
		#endregion
	}

	public static class TargetSettingsMockExtensions
	{
		#region Public Methods

		public static ITargetSettings WithToolChain( this ITargetSettings targetSettings, IToolChain toolChain )
		{
			Mock.Arrange( () => targetSettings.ToolChain )
				.Returns( toolChain );
			return targetSettings;
		}

		#endregion
	}
}