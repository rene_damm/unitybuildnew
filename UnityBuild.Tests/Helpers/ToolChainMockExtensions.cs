﻿using Telerik.JustMock;
using UnityBuild.Tools;

namespace UnityBuild.Tests.Helpers
{
	public static class ToolChainMockExtensions
	{
		#region Public Methods

		public static IToolChain WithTool< TTool >( this IToolChain toolChain, TTool tool )
			where TTool : ITool
		{
			Mock.Arrange( () => toolChain.GetTool< TTool >() )
				.Returns( tool );
			return toolChain;
		}

		public static IToolChain WithToolSettings< TToolSettings >( this IToolChain toolChain, TToolSettings toolSettings )
			where TToolSettings : IToolSettings
		{
			Mock.Arrange( () => toolChain.GetToolSettings< TToolSettings >() )
				.Returns( toolSettings );
			return toolChain;
		}

		#endregion
	}
}