﻿using System.Linq;
using NUnit.Framework;
using Telerik.JustMock;
using UnityBuild.Files;
using UnityBuild.Tools;

namespace UnityBuild.Tests.Helpers
{
	public static class ToolExecutionMockExtensions
	{
		#region Public Methods

		public static IToolExecution MustBeExecuted( this IToolExecution toolExecution )
		{
			Mock.Arrange( () => toolExecution.Tool.Run( Arg.Is( toolExecution.ToolInputs ) ) ).MustBeCalled();
			toolExecution.MustComplete();
			return toolExecution;
		}

        public static IToolExecution MustBeExecutedAfter( this IToolExecution toolExecution, IToolExecution otherToolExecution )
        {
            var otherToolExecutionHasRun = false;

			Mock.Arrange( () => otherToolExecution.Tool.Run( Arg.Is( otherToolExecution.ToolInputs ) ) )
                .DoInstead( () => otherToolExecutionHasRun = true )
                .MustBeCalled();

            Mock.Arrange( () => toolExecution.Tool.Run( Arg.Is( toolExecution.ToolInputs ) ) )
                .DoInstead( () => Assert.That( otherToolExecutionHasRun ) )
                .MustBeCalled();

			otherToolExecution.MustComplete();
			toolExecution.MustComplete();

			return toolExecution;
        }

		public static IToolExecution MustNotBeExecuted( this IToolExecution toolExecution )
		{
			Mock.Arrange( () => toolExecution.Tool.Run( Arg.IsAny< IToolInputs >() ) ).OccursNever();
			return toolExecution;
		}

		public static IToolExecution MustBeFoundUnstable( this IToolExecution toolExecution )
		{
			Mock.ArrangeSet( () => toolExecution.ToolInputs.Stable = false );
			return toolExecution;
		}

		public static IToolExecution MustBeFoundStable( this IToolExecution toolExecution )
		{
			Mock.ArrangeSet( () => toolExecution.ToolInputs.Stable = true );
			return toolExecution;
		}

        public static IToolExecution WithInputFiles( this IToolExecution toolExecution, params IFile[] files )
		{
			Mock.Arrange( () => toolExecution.ToolInputs.InputFiles ).Returns( files );
			return toolExecution;
		}

		public static IToolExecution WithOutputFiles( this IToolExecution toolExecution, params IFile[] files )
		{
			Mock.Arrange( () => toolExecution.ToolInputs.OutputFiles ).Returns( files );
			return toolExecution;
		}

		public static IToolExecution WithoutInputFiles( this IToolExecution toolExecution )
		{
			Mock.Arrange( () => toolExecution.ToolInputs.InputFiles ).Returns( Enumerable.Empty< IFile >() );
			return toolExecution;
		}

		public static IToolExecution WithoutOutputFiles( this IToolExecution toolExecution )
		{
			Mock.Arrange( () => toolExecution.ToolInputs.OutputFiles ).Returns( Enumerable.Empty< IFile >() );
			return toolExecution;
		}

		#endregion
	}
}