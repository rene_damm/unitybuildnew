﻿using System.Linq;
using NUnit.Framework;
using Telerik.JustMock;
using UnityBuild.Artifacts;
using UnityBuild.Builds;
using UnityBuild.Files;
using UnityBuild.Targets;
using UnityBuild.Tools;
using UnityBuild.Utilities;

namespace UnityBuild.Tests.Helpers
{
	[ TestFixture ]
	public abstract class BaseTestFixture
	{
		#region Public Methods

		/// <summary>
		/// Create a test object where we set up specific interactions.  Everything that is not
		/// arranged explicitly on the object will throw an exception during the test.
		/// </summary>
		/// <typeparam name="T">Type to mock.</typeparam>
		/// <returns>An uninitialized mock.</returns>
		public static T NewMock< T >()
		{
			return Mock.Create< T >( Behavior.Strict );
		}

		/// <summary>
		/// <para>Create a test object for which we don't care about interactions.  You may still arrange
		/// interactions to do state-based testing, but everything has defaults and all calls will
		/// be accepted.</para>	
		/// 
		/// <para>In short, create an object that helps testing but will not cause a test to fail.</para>
		/// </summary>
		/// <typeparam name="T">Type to stub.</typeparam>
		/// <returns>A fully initialized stub.</returns>
		public static T NewStub< T >()
		{
			return Mock.Create< T> ( Behavior.RecursiveLoose );
		}

		public static ITarget NewTarget()
		{
			return NewMock< ITarget >()
				.HasNotCompleted()
				.MustComplete()
				.WithoutDependencies(); // No dependencies by default.
		}

		public static ITargetSettings NewTargetSettings()
		{
			return NewMock< ITargetSettings >()
				.WithToolChain( NewMock< IToolChain >() );
		}

		public static IBuildInputs NewBuildWithSingleBuildStep( params IToolExecution[] toolExecutions )
		{
			return NewMock< IBuildInputs >()
				.WithBuildSteps
				(
					NewMock< IBuildStep >()
						.WithToolExecutions( toolExecutions )
						.MustComplete()
				);
		}

		public static IBuildFiles NewBuildFiles()
		{
			var buildFiles = NewMock< IBuildFiles >();
			var sourceFiles = NewMock< IFileCollection >();
			var intermediateFiles = NewMock< IFileCollection >();
			var outputFiles = NewMock< IFileCollection >();
			var fileSystem = NewFileSystem();

			Mock.Arrange( () => buildFiles.FileSystem ).Returns( fileSystem );
			Mock.Arrange( () => buildFiles.Sources ).Returns( sourceFiles );
			Mock.Arrange( () => buildFiles.Intermediates ).Returns( intermediateFiles );
			Mock.Arrange( () => buildFiles.Outputs ).Returns( outputFiles );

			return buildFiles;
		}

		public static IFileSystem NewFileSystem()
		{
			var fileSystem = NewMock< IFileSystem >();

			Mock.Arrange( () => fileSystem.ListFiles( Arg.AnyString ) )
				.Returns( Enumerable.Empty< string >() );
			Mock.Arrange( () => fileSystem.ListDirectories( Arg.AnyString ) )
				.Returns( Enumerable.Empty< string >() );

			return fileSystem;
		}

		public static IFile NewFile()
		{
			return NewMock< IFile >()
				.WithHash( null ) // No hash by default.
				.WithoutIncludes() // No includes by default.
				.OwnableByAny< IFileCollection, IFile >()
				.UseCache();
		}

		public static IFile NewFileWithPath( string path )
		{
			return NewFile()
				.WithPath( path );
		}

		public static IArtifact NewArtifact( IHash hash )
		{
			return NewMock< IArtifact >()
				.WithHash( hash )
				.WithPath( hash.ToString() );
		}

		#endregion
	}
}