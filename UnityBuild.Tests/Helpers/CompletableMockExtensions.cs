﻿using NUnit.Framework;
using Telerik.JustMock;
using UnityBuild.Utilities;

namespace UnityBuild.Tests.Helpers
{
	public static class CompletableMockExtensions
	{
		#region Public Methods

		public static T HasNotCompleted< T >( this T completable )
			where T : ICompletable
		{
			Mock.Arrange( () => completable.IsComplete ).Returns( false );
			return completable;
		}

		public static T MustComplete< T >( this T completable )
			where T : ICompletable
		{
			Mock.ArrangeSet( () => completable.IsComplete = true );
			return completable;
		}

		public static T MustCompleteAfter< T >( this T completable, ICompletable otherCompletable )
			where T : ICompletable
		{
			var otherHasCompleted = false;

			Mock.ArrangeSet( () => otherCompletable.IsComplete = true )
				.DoInstead( () => otherHasCompleted = true )
				.MustBeCalled();

			Mock.ArrangeSet( () => completable.IsComplete = true )
				.DoInstead( () => Assert.That( otherHasCompleted ) )
				.MustBeCalled();

			return completable;
		}

		#endregion
	}
}