﻿using Telerik.JustMock;
using UnityBuild.Artifacts;
using UnityBuild.Utilities;

namespace UnityBuild.Tests.Helpers
{
	public static class ArtifactCacheMockExtensions
	{
		#region Public Methods

		public static IArtifactCache HasArtifact( this IArtifactCache artifactCache, IArtifact artifact )
		{
			Mock.Arrange( () => artifactCache.FindArtifact( Arg.Is( artifact.Hash ) ) ).Returns( artifact );
			return artifactCache;
		}

		public static IArtifactCache MustCreateArtifact( this IArtifactCache artifactCache, IArtifact artifact )
		{
			Mock.Arrange( () => artifactCache.CreateArtifact( artifact.Hash ) )
				.Returns( artifact )
				.MustBeCalled();
			return artifactCache;
		}

		public static IArtifactCache MustNotCreateArtifactWithHash( this IArtifactCache artifactCache, IHash hash )
		{
			Mock.Arrange( () => artifactCache.CreateArtifact( hash ) ).OccursNever();
			return artifactCache;
		}
		
		#endregion
	}
}