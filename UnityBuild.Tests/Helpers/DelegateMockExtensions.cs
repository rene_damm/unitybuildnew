﻿using System;
using Telerik.JustMock;
using Telerik.JustMock.Helpers;

namespace UnityBuild.Tests.Helpers
{
	public static class DelegateMockExtensions
	{
		#region Public Methods

		public static Func< TResult > Returns< TResult >( this Func< TResult > func, TResult value )
		{
			Mock.Arrange( () => func() )
				.Returns( value )
				.MustBeCalled();
			return func;
		}

		public static Func< T1, TResult > Returns< T1, TResult >( this Func< T1, TResult > func, TResult value )
		{
			Mock.Arrange( () => func( Arg.IsAny< T1 >() ) )
				.Returns( value )
				.MustBeCalled();
			return func;
		}

		public static Func< TResult > Returns< TResult >( this Func< TResult > func, Func< TResult > @delegate )
		{
			Mock.Arrange( () => func() )
				.Returns( @delegate )
				.MustBeCalled();
			return func;
		}

		public static Func< TResult > Returns< TResult >( this Func< TResult > func, params TResult[] values )
		{
			Mock.Arrange( () => func() )
				.ReturnsMany( values )
				.MustBeCalled();
			return func;
		}

		public static Func< T1, TResult > Returns< T1, TResult >( this Func< T1, TResult > func, params Func< T1, TResult >[] values )
		{
			foreach ( var value in values )
			{
				var valueFunc = value;
				Mock.Arrange( () => func( Arg.IsAny< T1 >() ) )
					.Returns( valueFunc )
					.InSequence()
					.MustBeCalled();
			}
			return func;
		}

		#endregion
	}
}