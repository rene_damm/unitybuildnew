﻿using Telerik.JustMock;
using UnityBuild.Artifacts;

namespace UnityBuild.Tests.Helpers
{
	public static class ArtifactMockExtensions
	{
		#region Public Methods

		public static TArtifact WithPath< TArtifact >( this TArtifact artifact, string path )
			where TArtifact : IArtifact
		{
			Mock.Arrange( () => artifact.Path ).Returns( path );
			return artifact;
		}

		#endregion
	}
}