﻿using System.IO;
using NUnit.Framework;

namespace UnityBuild.Tests.Helpers
{
	[ Category( "FunctionalTests" ) ]
	public abstract class FunctionalTestFixture
		: BaseTestFixture
	{
		#region Non-Public Properties

		protected string WorkingCopyPath
		{
			////TODO: properly detect path to working copy
			get { return @"C:\Unity"; }
		}

		protected string BuildOutputPath
		{
			get { return Path.Combine( WorkingCopyPath, "build" ); }
		}

		#endregion
	}
}