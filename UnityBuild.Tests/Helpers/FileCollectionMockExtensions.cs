﻿using Telerik.JustMock;
using UnityBuild.Files;

namespace UnityBuild.Tests.Helpers
{
	public static class FileCollectionMockExtensions
	{
		#region Public Methods

		public static IFileCollection HasAnyFile( this IFileCollection fileCollection )
		{
			Mock.Arrange( () => fileCollection.GetFile( Arg.AnyString ) )
				.Returns( ( string path ) => BaseTestFixture.NewMock< IFile >().WithPath( path ) )
				.MustBeCalled();
			return fileCollection;
		}

		public static IFileCollection HasFile( this IFileCollection fileCollection, IFile file )
		{
			var path = file.Path;
			Mock.Arrange( () => fileCollection.GetFile( path ) )
				.Returns( file )
				.MustBeCalled();
				
			return fileCollection;
		}

		#endregion
	}
}