﻿using System.Linq;
using Telerik.JustMock;
using UnityBuild.Builds;
using UnityBuild.Tools;

namespace UnityBuild.Tests.Helpers
{
	public static class BuildStepMockExtensions
	{
		#region Public Methods

		public static IBuildStep WithToolExecution( this IBuildStep buildStep, IToolExecution toolExecution )
		{
			Mock.Arrange( () => buildStep.ToolExecutions ).Returns( new[] { toolExecution } );
			return buildStep;
		}

		public static IBuildStep WithToolExecutions( this IBuildStep buildStep, params IToolExecution[] toolExecutions )
		{
			Mock.Arrange( () => buildStep.ToolExecutions ).Returns( toolExecutions );
			return buildStep;
		}

		public static IBuildStep WithoutToolExecutions( this IBuildStep buildStep )
		{
			Mock.Arrange( () => buildStep.ToolExecutions ).Returns( Enumerable.Empty< IToolExecution >() );
			return buildStep;
		}

		#endregion
	}
}