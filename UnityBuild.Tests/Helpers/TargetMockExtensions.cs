﻿using System.Linq;
using Telerik.JustMock;
using UnityBuild.Builds;
using UnityBuild.Targets;

namespace UnityBuild.Tests.Helpers
{
	public static class TargetMockExtensions
	{
		#region Public Methods

		public static ITarget IgnoreInitialize( this ITarget target )
		{
			Mock.Arrange( () => target.Initialize( Arg.AnyString, Arg.IsAny< ITargetSettings >(), Arg.IsAny< IBuildFiles >() ) )
				.IgnoreInstance();
			return target;
		}
		
		public static ITarget MustBeInitializedWith( this ITarget target, string name, ITargetSettings settings, IBuildFiles buildFiles )
		{
			Mock.Arrange( () => target.Initialize( name, settings, buildFiles ) )
				.MustBeCalled();
			return target;
		}

		public static ITarget WithSettings( this ITarget target, ITargetSettings settings )
		{
			Mock.Arrange( () => target.Settings ).Returns( settings );
			return target;
		}

        public static ITarget WithBuildSteps( this ITarget target, params IBuildStep[] buildSteps )
        {
            Mock.Arrange( () => target.BuildSteps ).Returns( buildSteps );
            return target;
        }

		public static IBuildStep WithSingleBuildPhase( this ITarget target )
		{
			var phase = Mock.Create< IBuildStep >( Behavior.Strict );
			Mock.Arrange( () => target.BuildSteps ).Returns( new[] { phase } );
			return phase;
		}

		public static ITarget WithoutBuildSteps( this ITarget target )
		{
			Mock.Arrange( () => target.BuildSteps ).Returns( Enumerable.Empty< IBuildStep >() );
			return target;
		}

		public static ITarget WithoutDependencies( this ITarget target )
		{
            Mock.Arrange( () => target.Dependencies ).Returns( Enumerable.Empty< ITargetDependency >() );
			return target;
		}

		#endregion
	}
}