﻿using Telerik.JustMock;
using UnityBuild.Builds;
using UnityBuild.Tools;

namespace UnityBuild.Tests.Helpers
{
	public static class BuildEngineMockExtensions
	{
		#region Public Methods

		public static IBuildEngine IgnoresAnyBuild( this IBuildEngine buildEngine )
		{
			Mock.Arrange( () => buildEngine.Build( Arg.IsAny< IBuildInputs >() ) )
				.IgnoreInstance();
			return buildEngine;
		}

		public static void Build( this IBuildEngine buildEngine, IToolExecution toolExecution )
		{
			var buildInputs = BaseTestFixture.NewBuildWithSingleBuildStep( toolExecution );
			buildEngine.Build( buildInputs );
		}

		#endregion
	}
}