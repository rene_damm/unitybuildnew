﻿using System.Linq;
using Telerik.JustMock;
using UnityBuild.Files;

namespace UnityBuild.Tests.Helpers
{
	public static class FileMockExtensions
	{
		#region Public Methods

		public static IFile WithIncludes( this IFile file, params IFile[] includes )
		{
			Mock.Arrange( () => file.Includes ).Returns( includes );
			return file;
		}

		public static IFile WithoutIncludes( this IFile file )
		{
			Mock.Arrange( () => file.Includes ).Returns( Enumerable.Empty< IFile >() );
			return file;
		}

		public static IFile DontCache( this IFile file )
		{
			Mock.Arrange( () => file.DontCache ).Returns( true );
			return file;
		}

		public static IFile UseCache( this IFile file )
		{
			Mock.Arrange( () => file.DontCache ).Returns( false );
			return file;
		}

		#endregion
	}
}