﻿using Autofac.Features.Indexed;
using NUnit.Framework;
using Telerik.JustMock;

namespace UnityBuild.Tests.Helpers
{
	public static class IndexMockExtensions
	{
		#region Public Methods

		public static IIndex< TKey, TValue > SetupDefaults< TKey, TValue >( this IIndex< TKey, TValue > index )
		{
			TValue value;
			Mock.Arrange( () => index.TryGetValue( Arg.IsAny< TKey >(), out value ) )
				.Returns( false );
			return index;
		}

		public static IIndex< TKey, TValue > Map< TKey, TValue >( this IIndex< TKey, TValue > index, TKey key, TValue value )
		{
			Mock.Arrange( () => index.TryGetValue( key, out value ) )
				.Returns( true )
				.MustBeCalled(); // No point in setting up mappings that aren't used in the test so we can assume here MustBeCalled() is always the right thing.
			return index;
		}

		public static IIndex< TKey, TValue > MapSequence< TKey, TValue >( this IIndex< TKey, TValue > index, TKey key, params TValue[] values )
		{
			var enumerator = values.GetEnumerator();

			TValue dummy;
			Mock.Arrange( () => index.TryGetValue( key, out dummy ) )
				.DoInstead(
					new TryGetValueDelegate< TKey, TValue >(
						( TKey k, out TValue result ) =>
						{
							if ( !enumerator.MoveNext() )
								Assert.Fail( "End of mocked value sequence for key '{0}' reached!  Not expecting any more lookups for this key!", k );

							result = ( TValue ) enumerator.Current;
						}
					)
				)
				.Returns( true );

			// Alternative and more elegant code that however fails to run properly due to
			// a reason I couldn't pin down.  Will fail every lookup on the given key for some
			// key types (Strings) and not for other types (Types).

			#if UNUSED_CODE
			foreach ( var value in values )
			{
				var result = value;
				Mock.Arrange( () => index.TryGetValue( key, out result ) )
					.Returns( true )
					.InSequence()
					.MustBeCalled();
			}

			// Terminate the sequence by making sure no further values
			// are requested for the given key.
			TValue dummy;
			Mock.Arrange( () => index.TryGetValue( key, out dummy ) )
				.InSequence()
				.OccursNever();
			#endif

			return index;
		}

		#endregion

		#region Inner Types

		private delegate void TryGetValueDelegate< in TKey, TValue >( TKey key, out TValue value );

		#endregion
	}
}