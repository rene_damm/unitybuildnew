﻿using FluentAssertions;
using NUnit.Framework;
using UnityBuild.Tests.Helpers;
using UnityBuild.Tools;

namespace UnityBuild.Tests.Tools
{
	/// <summary>
	/// Unit tests for <see cref="ToolChain"/>.
	/// </summary>
	public class ToolChainTests
		: UnitTestFixture
	{
		#region Public Methods

		[ SetUp ]
		public void SetUp()
		{
			ToolChain = new ToolChain();
		}

		/// <summary>
		/// Make sure we enter all tools we pass to the constructor into the table.
		/// </summary>
		[ Test ]
		public void Constructor_AddsToolsToTable()
		{
			// Arrange.
			var tool1 = new FakeTool1();
			var tool2 = new FakeTool2();
			var toolSettings1 = new FakeToolSettings1();
			var toolSettings2 = new FakeToolSettings2();

			// Act.
			var toolChain = new ToolChain(
				  new ITool[] { tool1, tool2 }
				, new IToolSettings[] { toolSettings1, toolSettings2 } );

			// Assert.
			toolChain.Tools.Values.Should().Contain( tool1 );
			toolChain.Tools.Values.Should().Contain( tool2 );
		}

		#endregion

		#region Non-Public Properties

		private ToolChain ToolChain { get; set; }

		#endregion

		#region Inner Types

		private class FakeTool1
			: Tool< FakeToolSettings1 >
		{
			#region Non-Public Methods

			protected override void InternalRun( IToolInputs inputs, FakeToolSettings1 settings )
			{
			}

			#endregion
		}

		private class FakeTool2
			: Tool< FakeToolSettings2 >
		{
			#region Non-Public Methods

			protected override void InternalRun( IToolInputs inputs, FakeToolSettings2 settings )
			{
			}

			#endregion
		}

		private class FakeToolSettings1
			: ToolSettings
		{
		}

		private class FakeToolSettings2
			: ToolSettings
		{
		}

		#endregion
	}
}