﻿using System.Linq;
using NUnit.Framework;
using UnityBuild.Files;
using UnityBuild.Tests.Helpers;
using UnityBuild.Tools;

namespace UnityBuild.Tests.Tools
{
    /// <summary>
    /// Unit tests for <see cref="UnityBuild.Tools.ToolExecution"/>.
    /// </summary>
    public class ToolExecutionTests
        : UnitTestFixture
    {
        #region Public Methods

        [ SetUp ]
        public void SetUp()
        {
			Tool = NewMock< ITool >();
            ToolSettings = NewMock< IToolSettings >();
            ToolExecution = new ToolExecution( Tool, ToolSettings );
        }

        /// <summary>
        /// Make sure that <see cref="UnityBuild.Tools.ToolExecution.AddInputFile"/> adds files
        /// to <see cref="UnityBuild.Tools.ToolExecution.InputFiles"/>.
        /// </summary>
        [ Test ]
        public void AddInputFile_AddsFileToInputFiles()
        {
            // Arrange.
            var file1 = NewMock< IFile >();
            var file2 = NewMock< IFile >();

            Assert.That( ToolExecution.InputFiles, Is.Empty );

            // Act.
            ToolExecution.AddInputFile( file1 );
            Assert.That( ToolExecution.InputFiles.Count(), Is.EqualTo( 1 ) );
            ToolExecution.AddInputFile( file2 );

            // Asset.
            Assert.That( ToolExecution.InputFiles.Count(), Is.EqualTo( 2 ) );
            Assert.That( ToolExecution.InputFiles, Has.Exactly( 1 ).SameAs( file1 ) );
            Assert.That( ToolExecution.InputFiles, Has.Exactly( 1 ).SameAs( file2 ) );
        }

        /// <summary>
        /// Make sure that <see cref="UnityBuild.Tools.ToolExecution.AddOutputFile"/> adds files
        /// to <see cref="UnityBuild.Tools.ToolExecution.OutputFiles"/>.
        /// </summary>
        [ Test ]
        public void AddOutputFile_AddsFileToOutputFiles()
        {
            // Arrange.
            var file1 = NewMock< IFile >();
            var file2 = NewMock< IFile >();

            Assert.That( ToolExecution.OutputFiles, Is.Empty );

            // Act.
            ToolExecution.AddOutputFile( file1 );
            Assert.That( ToolExecution.OutputFiles.Count(), Is.EqualTo( 1 ) );
            ToolExecution.AddOutputFile( file2 );

            // Asset.
            Assert.That( ToolExecution.OutputFiles.Count(), Is.EqualTo( 2 ) );
            Assert.That( ToolExecution.OutputFiles, Has.Exactly( 1 ).SameAs( file1 ) );
            Assert.That( ToolExecution.OutputFiles, Has.Exactly( 1 ).SameAs( file2 ) );
        }

		/// <summary>
        /// Make sure that <see cref="UnityBuild.Tools.ToolExecution.InputFiles"/> is not null
        /// in default state.
        /// </summary>
        [ Test ]
        public void InputFiles_IsNotNull()
        {
            Assert.That( ToolExecution.InputFiles, Is.Not.Null );
        }

		/// <summary>
        /// Make sure that <see cref="UnityBuild.Tools.ToolExecution.OutputFiles"/> is not null
        /// in default state.
        /// </summary>
        [ Test ]
        public void OutputFiles_IsNotNull()
        {
            Assert.That( ToolExecution.OutputFiles, Is.Not.Null );
        }

        #endregion

        #region Non-Public Properties

	    private ToolExecution ToolExecution { get; set; }

		/// <summary>
		/// Mock tool instance we feed into <see cref="ToolExecution"/>.
		/// </summary>
		private ITool Tool { get; set; }

		/// <summary>
		/// Mock tool settings we feed into <see cref="ToolSettings"/>.
		/// </summary>
		private IToolSettings ToolSettings { get; set; }

        #endregion
    }
}