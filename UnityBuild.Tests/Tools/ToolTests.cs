﻿using System;
using NUnit.Framework;
using UnityBuild.Tests.Helpers;
using UnityBuild.Tools;

namespace UnityBuild.Tests.Tools
{
	/// <summary>
	/// Unit tests for <see cref="Tool"/>.
	/// </summary>
	public class ToolTests
		: UnitTestFixture
	{
		#region Public Methods

		[ SetUp ]
		public void SetUp()
		{
			Tool = NewMock< Tool< IOurToolSettings > >();
		}

		/// <summary>
		/// Make sure we don't accept inputs to be null.
		/// </summary>
		[ Test ]
		public void Run_ThrowsIfInputsAreNull()
		{
			Assert.Throws< ArgumentException >( () => Tool.Run( null ) );
		}

		[ Test ]
		public void Run_ThrowsIfToolSettingsAreNull()
		{
			// Arrange.
			var inputs = NewMock< IToolInputs >()
				.WithSettings( null );

			// Act.
			Assert.Throws< ArgumentException >( () => Tool.Run( inputs ) );
		}

		/// <summary>
		/// Make sure we check whether we get settings of the right type.
		/// </summary>
		[ Test ]
		public void Run_ThrowsIfSettingsHaveIncorrectType()
		{
			// Arrange.
			var inputs = NewMock< IToolInputs >()
				.WithSettings( NewMock< ISomeOtherToolSettings >() );

			// Act.
			Assert.Throws< ArgumentException >( () => Tool.Run( inputs ) );
		}

		#endregion

		#region Non-Public Properties

		private ITool Tool { get; set; }

		#endregion

		#region Inner Types

// ReSharper disable MemberCanBePrivate.Global

		public interface IOurToolSettings
			: IToolSettings
		{
		}

		public interface ISomeOtherToolSettings
			: IToolSettings
		{
		}

// ReSharper restore MemberCanBePrivate.Global

		#endregion
	}
}