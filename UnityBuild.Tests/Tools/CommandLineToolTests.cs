﻿using System;
using System.Diagnostics;
using NUnit.Framework;
using Telerik.JustMock;
using UnityBuild.Tests.Helpers;
using UnityBuild.Tools;
using UnityBuild.Utilities;

namespace UnityBuild.Tests.Tools
{
	/// <summary>
	/// Unit tests for <see cref="CommandLineTool"/>.
	/// </summary>
	public class CommandLineToolTests
		: UnitTestFixture
	{
		#region Public Methods

		[ SetUp ]
		public void SetUp()
		{
			ProcessFactory = NewMock< IProcessFactory >();
			CommandLineTool = new TestCommandLineTool( ProcessFactory );
			CommandLineToolSettings = new CommandLineToolSettings();
			ToolInputs = NewMock< IToolInputs >();

			// Use our tool settings.
			Mock.Arrange( () => ToolInputs.ToolSettings ).Returns( CommandLineToolSettings );
		}

		/// <summary>
		/// Make sure that Run() spawns a process with the right executable.
		/// </summary>
		[ Test ]
		public void Run_LaunchesProcessWithExecutable()
		{
			// Arrange.
			const string executablePath = "executable";
			CommandLineTool.ExecutablePath = executablePath;

			Mock.Arrange( () => ProcessFactory.NewProcess( Arg.IsAny< ProcessStartInfo >() ) )
				.DoInstead(
					( ProcessStartInfo startInfo ) =>
					{
						Assert.That( startInfo.FileName, Is.EqualTo( executablePath ) );
					} )
				.MustBeCalled();

			// Act.
			CommandLineTool.Run( ToolInputs );

			// Assert.
			Mock.Assert( ProcessFactory );
		}

		/// <summary>
		/// Make sure <see cref="UnityBuild.Tools.CommandLineTool.ExecutablePath"/> has to be set.
		/// </summary>
		[ Test ]
		public void Run_WithNoExecutablePathThrows()
		{
			CommandLineTool.ExecutablePath = "";
			Assert.Throws< InvalidOperationException >( () => CommandLineTool.Run( ToolInputs ) );

			CommandLineTool.ExecutablePath = null;
			Assert.Throws< InvalidOperationException >( () => CommandLineTool.Run( ToolInputs ) );
		}

		#endregion

		#region Non-Public Properties

		private IProcessFactory ProcessFactory { get; set; }

		private CommandLineTool CommandLineTool { get; set; }

		private CommandLineToolSettings CommandLineToolSettings { get; set; }

		private IToolInputs ToolInputs { get; set; }

		#endregion

		#region Inner Types

		/// <summary>
		/// Dummy class to test <see cref="CommandLineTool"/>.  Simply using the mocking framework
		/// to synthesize would be a better choice except that JustMock is very greedy in mocking out
		/// non-public method and we can neither prevent it from doing nor can we actually properly
		/// set up things with the Lite edition.
		/// </summary>
		private class TestCommandLineTool
			: CommandLineTool
		{
			#region Constructors

			public TestCommandLineTool( IProcessFactory processFactory )
				: base( processFactory )
			{
			}

			#endregion
		}

		#endregion
	}
}