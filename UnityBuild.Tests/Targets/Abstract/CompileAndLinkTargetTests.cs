﻿using NUnit.Framework;
using Telerik.JustMock;
using UnityBuild.Builds;
using UnityBuild.Files;
using UnityBuild.Targets;
using UnityBuild.Targets.Abstract;
using UnityBuild.Tests.Helpers;
using UnityBuild.Tools.Cpp;

namespace UnityBuild.Tests.Targets.Abstract
{
	/// <summary>
	/// Unit tests for <see cref="CompileAndLinkTarget"/>.
	/// </summary>
	public class CompileAndLinkTargetTests
		: UnitTestFixture
	{
		#region Public Methods

		[ SetUp ]
		public void SetUp()
		{
			Compiler = NewMock< CppCompiler >();
			Linker = NewMock< CppLinker >();
			CompilerSettings = NewMock< CppCompilerSettings >();
			LinkerSettings = NewMock< CppLinkerSettings >();

			BuildFiles = NewBuildFiles();
			TargetSettings = NewTargetSettings();
			TargetSettings.ToolChain
				.WithTool( Compiler )
				.WithTool( Linker )
				.WithToolSettings( CompilerSettings )
				.WithToolSettings( LinkerSettings );

			CompileAndLinkTarget = Mock.Create< CompileAndLinkTarget >( Constructor.NotMocked, Behavior.CallOriginal );
			CompileAndLinkTarget.Initialize( "Test", TargetSettings, BuildFiles );
		}

		/// <summary>
		/// Make sure that <see cref="UnityBuild.Targets.Abstract.CompileAndLinkTarget.Compile"/> creates a new source
		/// file from a path given to it.
		/// </summary>
		[ Test ]
		public void Compile_CreatesSourceFileFromPath()
		{
			// Arrange.
			BuildFiles.Sources.HasAnyFile();
			//BuildFiles.Intermediates.HasAnyFile(); ////FIXME: this triggers a bug in JustMock
			BuildFiles.FileSystem.HasFileListing( "test", "test" );

			////WORKAROUND: Replace the strict mock for BuildFiles.Intermediates with a loose
			////  mock so we don't need to arrange the GetFile() call; doing so will make JustMock
			////  trigger a NotImplementedException in DynamicProxy2.
			var intermediates = Mock.Create< IFileCollection >();
			Mock.Arrange( () => BuildFiles.Intermediates ).Returns( intermediates );

			// Act.
			CompileAndLinkTarget.Compile( "test" );
			
			// Assert.
			Mock.Assert( BuildFiles );
		}

		#endregion

		#region Non-Public Properties

		private CompileAndLinkTarget CompileAndLinkTarget { get; set; }

		private ITargetSettings TargetSettings { get; set; }

		private IBuildFiles BuildFiles { get; set; }

		private CppCompiler Compiler { get; set; }

		private CppLinker Linker { get; set; }

		private CppCompilerSettings CompilerSettings { get; set; }

		private CppLinkerSettings LinkerSettings { get; set; }

		#endregion
	}
}