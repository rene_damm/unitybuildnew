﻿using System;
using System.Collections.Generic;
using System.Linq;
using Autofac.Features.Indexed;
using NUnit.Framework;
using Telerik.JustMock;
using UnityBuild.Builds;
using UnityBuild.Targets;
using UnityBuild.Tests.Helpers;

namespace UnityBuild.Tests.Targets
{
	using TargetConstructor = Func< ITarget >;

	/// <summary>
    /// Unit tests for <see cref="TargetCollection"/>.
    /// </summary>
	public class TargetCollectionTests
		: UnitTestFixture
	{
		#region Public Methods

		[ SetUp ]
		public void SetUp()
		{
			TargetConstructors = NewMock< IIndex< string, TargetConstructor > >()
				.SetupDefaults();

			BuildFiles = NewMock< IBuildFiles >();
			TargetCollection = new TargetCollection( BuildFiles, TargetConstructors );
		}
		
		/// <summary>
		/// Make sure we successfully create a target we have a constructor for.
		/// </summary>
		[ Test ]
		public void FindOrAddTargetByName_CreatesKnownTarget()
		{
			// Arrange.
			const string targetName = "Test";
			var settings = NewMock< ITargetSettings >();
			var target = NewMock< ITarget >()
				.MustBeInitializedWith( targetName, settings, BuildFiles );

			TargetConstructors.Map( targetName, () => target );

			// Act.
			var result = TargetCollection.FindOrAddTargetByName( targetName, settings );

			// Assert.
			Assert.That( result, Is.SameAs( target ) );
			Assert.That( TargetCollection.Count, Is.EqualTo( 1 ) );
			Assert.That( TargetCollection.First(), Is.SameAs( target ) );
			Mock.Assert( TargetConstructors );
			Mock.Assert( target );
		}

		/// <summary>
		/// Make sure that if we already have a target with the same name and settings,
		/// we return that instead of creating a new instance.
		/// </summary>
		[ Test ]
		public void FindOrAddTargetByName_ReturnsExistingTargetWithSameSettings()
		{
			// Arrange.
			const string targetName = "Test";
			var settings = NewMock< ITargetSettings >();
			var target = NewMock< ITarget >()
				.WithName( targetName )
				.WithSettings( settings )
				.MustBeInitializedWith( targetName, settings, BuildFiles );

			TargetConstructors.MapSequence( targetName, () => target );
			TargetCollection.FindOrAddTargetByName( targetName, settings );

			// Act.
			var result = TargetCollection.FindOrAddTargetByName( targetName, settings );

			// Assert.
			Assert.That( result, Is.SameAs( target ) );
			Mock.Assert( TargetConstructors );
		}

		/// <summary>
		/// Make sure we throw if we get a target we don't know.
		/// </summary>
		[ Test ]
		public void FindOrAddTargetByName_ThrowsIfTargetNotKnown()
		{
			Assert.Throws< KeyNotFoundException >( () => TargetCollection.FindOrAddTargetByName( "DoesNotExist", NewMock< ITargetSettings >() ) );
		}

		/// <summary>
		/// Make sure we expect a target name.
		/// </summary>
		[ Test ]
		public void FindOrAddTargetByName_ThrowsIfTargetNameIsNullOrEmpty()
		{
			Assert.Throws< ArgumentException >( () => TargetCollection.FindOrAddTargetByName( "", NewMock< ITargetSettings >() ) );
			Assert.Throws< ArgumentException >( () => TargetCollection.FindOrAddTargetByName( null, NewMock< ITargetSettings >() ) );
		}

		[ Test ]
		public void FindOrAddTargetByName_ThrowsIfTargetSettingsIsNull()
		{
			Assert.Throws< ArgumentException >( () => TargetCollection.FindOrAddTargetByName( "foo", null ) );
		}

		#endregion

		#region Non-Public Properties

		/// <summary>
		/// <see cref="TargetCollection"/> we are testing.
		/// </summary>
		private TargetCollection TargetCollection { get; set; }

		/// <summary>
		/// Mock set of build files we use with the collection.
		/// </summary>
		private IBuildFiles BuildFiles { get; set; }

		/// <summary>
		/// Mock index we set up to return constructors.
		/// </summary>
		private IIndex< string, TargetConstructor > TargetConstructors { get; set; }
		 
		#endregion
	}
}