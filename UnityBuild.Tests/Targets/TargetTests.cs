﻿using System;
using NUnit.Framework;
using Telerik.JustMock;
using UnityBuild.Builds;
using UnityBuild.Targets;
using UnityBuild.Tests.Helpers;
using UnityBuild.Utilities;

namespace UnityBuild.Tests.Targets
{
	/// <summary>
	/// Unit tests for <see cref="Target"/>.
	/// </summary>
	[ TestFixture ]
	public class TargetTests
		: UnitTestFixture
	{
		#region Public Methods

		[ SetUp ]
		public void SetUp()
		{
			Target = Mock.Create< Target >( Constructor.NotMocked, Behavior.Loose );
		}

		/// <summary>
		/// Make sure we require target settings to be handed to a target.
		/// </summary>
		[ Test ]
		public void Initialize_ThrowsIfNotGivenAnySettings()
		{
			Assert.Throws< ArgumentException >( () => Target.Initialize( "test", null, NewMock< IBuildFiles >() ) );
		}

		/// <summary>
		/// Make sure we require build inputs to be handed to a target.
		/// </summary>
		[ Test ]
		public void Initialize_ThrowsIfNotGivenBuildInputs()
		{
			Assert.Throws< ArgumentException >( () => Target.Initialize( "test", NewMock< ITargetSettings >(), null ) );
		}

		/// <summary>
		/// Make sure a target can only be initialize once.
		/// </summary>
		[ Test ]
		public void Initialize_CanOnlyBeCalledOnce()
		{
			Target.Initialize( "test", NewMock< ITargetSettings >(), NewMock< IBuildFiles >() );
			Assert.Throws< InvalidOperationException >( () => Target.Initialize( "test", NewMock< ITargetSettings >(), NewMock< IBuildFiles >() ) );
		}

		/// <summary>
		/// Make sure a target raises its <see cref="ICompletable.OnComplete"/> event once all
		/// its build steps are complete.
		/// </summary>
		[ Test ]
		public void OnComplete_RaisedWhenAllBuildStepsAreComplete()
		{
			// Arrange.
			var buildStep1 = Target.AddBuildStep( "1" );
			var buildStep2 = Target.AddBuildStep( "2" );
			var didRaiseOnComplete = false;
			Target.OnComplete += _ => didRaiseOnComplete = true;

			// Act.
			buildStep1.IsComplete = true;
			Assert.That( didRaiseOnComplete, Is.False );
			buildStep2.IsComplete = true;

			// Assert.
			Assert.That( didRaiseOnComplete, Is.True  );
		}

		/// <summary>
		/// Make sure <see cref="UnityBuild.Targets.Target.AddBuildStep"/> appends a new build phase on each call.
		/// </summary>
		[ Test ]
		public void AddBuildStep_AppendsNewStep()
		{
			Target.AddBuildStep( "1" );
			Target.AddBuildStep( "2" );

			Assert.That( Target.BuildSteps.Count, Is.EqualTo( 2 ) );
			Assert.That( Target.BuildSteps[ 0 ].Name, Is.EqualTo( "1" ) );
			Assert.That( Target.BuildSteps[ 1 ].Name, Is.EqualTo( "2" ) );
		}

		#endregion

		#region Non-Public Properties

		private Target Target { get; set; }

		#endregion
	}
}