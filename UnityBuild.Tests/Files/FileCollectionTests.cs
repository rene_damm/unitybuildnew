﻿using System;
using System.Linq;
using NUnit.Framework;
using Telerik.JustMock;
using UnityBuild.Files;
using UnityBuild.Tests.Helpers;

namespace UnityBuild.Tests.Files
{
	/// <summary>
	/// Unit tests for <see cref="FileCollection"/>.
	/// </summary>
	public class FileCollectionTests
		: UnitTestFixture
	{
		#region Public Methods

		[ SetUp ]
		public void SetUp()
		{
			FileConstructor = NewMock< Func< IFile > >();
			FileCollection = new FileCollection( "test", FileConstructor, null );
		}

		/// <summary>
		/// Make sure that when we don't give a path, we always return a new file.
		/// </summary>
		[ Test ]
		public void GetFile_CreatesNewFileWhenNotGivenPath()
		{
			// Arrange.
			var file1 = NewFile();
			var file2 = NewFile();

			FileConstructor.Returns(
				  file1
				, file2 );

			// Act.
			var result1 = FileCollection.GetFile();
			var result2 = FileCollection.GetFile();

			// Assert.
			Mock.Assert( FileConstructor );
			Assert.That( result1, Is.SameAs( file1 ) );
			Assert.That( result2, Is.SameAs( file2 ) );
			Assert.That( FileCollection.Count, Is.EqualTo( 2 ) );
			Assert.That( FileCollection.Paths, Is.Empty );
			Assert.That( FileCollection.Contains( result1 ) );
			Assert.That( FileCollection.Contains( result2 ) );
		}

		/// <summary>
		/// Make sure that when we use a path not seen by the collection before, it returns
		/// a new file.
		/// </summary>
		[ Test ]
		public void GetFile_CreatesNewFileWhenGivenNewPath()
		{
			// Arrange.
			FileConstructor.Returns( NewFile );

			// Act.
			var result1 = FileCollection.GetFile( "test1" );
			var result2 = FileCollection.GetFile( "test2" );

			// Assert.
			Assert.That( result1, Is.Not.SameAs( result2 ) );
			Assert.That( FileCollection.Count, Is.EqualTo( 2 ) );
			Assert.That( FileCollection.Paths.Count, Is.EqualTo( 2 ) );
			Assert.That( FileCollection.Paths[ "test1" ], Is.SameAs( result1 ) );
			Assert.That( FileCollection.Paths[ "test2" ], Is.SameAs( result2 ) );
			Assert.That( FileCollection.Contains( result1 ) );
			Assert.That( FileCollection.Contains( result2 ) );
		}

		/// <summary>
		/// Make sure that when we use a path the collection has seen before, we return the
		/// file we constructed previously.
		/// </summary>
		[ Test ]
		public void GetFile_ReturnsExistingFileWhenGivenKnownPath()
		{
			// Arrange.
			var file = NewFile();
			FileConstructor.Returns( file );

			// Act.
			var result1 = FileCollection.GetFile( "test" );
			var result2 = FileCollection.GetFile( "test" );

			// Assert.
			Assert.That( result1, Is.SameAs( result2 ) );
			Assert.That( FileCollection.Count, Is.EqualTo( 1 ) );
			Assert.That( FileCollection.Paths.Count, Is.EqualTo( 1 ) );
			Assert.That( FileCollection.Paths[ "test" ], Is.SameAs( result1 ) );
			Assert.That( FileCollection.Contains( result1 ) );
		}

		/// <summary>
		/// Make sure we own the files we create.
		/// </summary>
		[ Test ]
		public void GetFile_SetsOwnerWhenCreatingFile()
		{
			// Arrange.
			var file = NewFile()
				.OwnableOnlyBy< IFileCollection, IFile >( FileCollection );
			FileConstructor.Returns( file );

			// Act.
			FileCollection.GetFile();

			// Assert.
			Mock.Assert( file );
		}

		#endregion

		#region Non-Public Properties

		private FileCollection FileCollection { get; set; }

		private Func< IFile > FileConstructor { get; set; }

		#endregion
	}
}