﻿using System;
using System.Linq;
using NUnit.Framework;
using Telerik.JustMock;
using UnityBuild.Files;
using UnityBuild.Tests.Helpers;

namespace UnityBuild.Tests.Files
{
	/// <summary>
	/// Unit tests for <see cref="FileSystemExtensions"/>.
	/// </summary>
	public class FileSystemExtensionsTests
		: UnitTestFixture
	{
		#region Public Methods

		[ SetUp ]
		public void SetUp()
		{
			FileSystem = Mock.Create< IFileSystem >( Behavior.Strict );
		}

		[ Test ]
		public void CopyFiles_InvokesCopyFileWithPairsOfPaths()
		{
			// Arrange.
			var from = new[] { "a", "b" };
			var to = new[] { "1", "2" };

			Mock.Arrange( () => FileSystem.CopyFile( Arg.Is( "a" ), Arg.Is( "1" ) ) ).MustBeCalled();
			Mock.Arrange( () => FileSystem.CopyFile( Arg.Is( "b" ), Arg.Is( "2" ) ) ).MustBeCalled();

			// Act.
			FileSystem.CopyFiles( from, to );

			// Assert.
			Mock.Assert( FileSystem );
		}

		[ Test ]
		public void CopyFiles_MismatchInListLengthThrowsArgumentException()
		{
			// Arrange.
			var from = new[] { "a" };
			var to = Enumerable.Empty< string >();

			// Act.
			Assert.Throws< ArgumentException >( () => FileSystem.CopyFiles( from, to ) );

			// Assert.
			Mock.Assert( FileSystem );
		}

		#endregion

		#region Non-Public Properties

		private IFileSystem FileSystem { get; set; }

		#endregion
	}
}