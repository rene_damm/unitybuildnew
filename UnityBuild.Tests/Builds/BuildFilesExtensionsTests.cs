﻿using System;
using NUnit.Framework;
using UnityBuild.Builds;
using UnityBuild.Tests.Helpers;

namespace UnityBuild.Tests.Builds
{
	/// <summary>
	/// Unit tests for <see cref="BuildFilesExtensions"/>.
	/// </summary>
	public class BuildFilesExtensionsTests
		: UnitTestFixture
	{
		#region Public Methods

		[ SetUp ]
		public void SetUp()
		{
			BuildFiles = NewBuildFiles();
		}

		/// <summary>
		/// Make sure <see cref="BuildFilesExtensions.GetSourceFiles{TFile}"/> doesn't accept not
		/// being given a path.
		/// </summary>
		[ Test ]
		public void GetSourceFiles_ThrowsIfNotGivenPath()
		{
			Assert.Throws< ArgumentException >( () => BuildFiles.GetSourceFiles( null ) );
			Assert.Throws< ArgumentException >( () => BuildFiles.GetSourceFiles( "" ) );
		}

		/// <summary>
		/// Make sure <see cref="BuildFilesExtensions.GetSourceFiles{TFile}"/> returns nothing if
		/// the path doesn't match any file on the file system.
		/// </summary>
		[ Test ]
		public void GetSourceFiles_ReturnsEmptyIfNoMatchingFile()
		{
			Assert.That( BuildFiles.GetSourceFiles( "Does Not Exist" ), Is.Empty );
		}

		/// <summary>
		/// Make sure <see cref="BuildFilesExtensions.GetSourceFiles{TFile}"/> turns every match
		/// found on the file system into a file.
		/// </summary>
		[ Test ]
		public void GetSourceFiles_ReturnsAllMatches()
		{
			// Arrange.
			var file1 = NewFileWithPath( "file1" );
			var file2 = NewFileWithPath( "file2" );

			BuildFiles.Sources
				.HasFile( file1 )
				.HasFile( file2 );

			BuildFiles.FileSystem.HasFileListing( "path", "file1", "file2" );

			// Act.
			var result = BuildFiles.GetSourceFiles( "path" );

			// Assert.
			Assert.That( result, Is.EquivalentTo( new[] { file1, file2 } ) );
		}

		#endregion

		#region Non-Public Properties

		private IBuildFiles BuildFiles { get; set; }

		#endregion
	}
}