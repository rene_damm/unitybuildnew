﻿using System;
using NUnit.Framework;
using UnityBuild.Builds;
using UnityBuild.Tests.Helpers;

namespace UnityBuild.Tests.Builds
{
	/// <summary>
	/// Unit tests for <see cref="BuildStep"/>.
	/// </summary>
	public class BuildStepTests
		: UnitTestFixture
	{
		#region Public Methods

		[ SetUp ]
		public void SetUp()
		{
			BuildStep = new BuildStep( "Test" );
		}

		/// <summary>
		/// Make sure the constructor forces names to be given to build steps.
		/// </summary>
		[ Test ]
		public void Constructor_ThrowsIfNoNameGiven()
		{
			Assert.Throws< ArgumentException >( () => new BuildStep( null ) );
			Assert.Throws< ArgumentException >( () => new BuildStep( "" ) );
		}

		#endregion

		#region Non-Public Properties

		private BuildStep BuildStep { get; set; }

		#endregion
	}
}