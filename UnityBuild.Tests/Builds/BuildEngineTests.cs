﻿using System;
using FluentAssertions;
using NUnit.Framework;
using Telerik.JustMock;
using UnityBuild.Artifacts;
using UnityBuild.Builds;
using UnityBuild.Files;
using UnityBuild.Tests.Helpers;
using UnityBuild.Tools;
using UnityBuild.Utilities;

////REVIEW: too much interaction testing in here; too much use of mocks; most of the tests test too many things are not directly related

namespace UnityBuild.Tests.Builds
{
	/// <summary>
	/// Unit tests for <see cref="BuildEngine"/>.
	/// </summary>
    public class BuildEngineTests
		: UnitTestFixture
	{
		#region Public Methods

		[ SetUp ]
		public void SetUp()
		{
			FileSystem = NewMock< IFileSystem >();
			ArtifactCache = NewMock< IArtifactCache >();
			BuildEngine = new BuildEngine( FileSystem, ArtifactCache );

			// For any path we don't match, return null from FileSystem.GetFileInfo.
			Mock.Arrange( () => FileSystem.GetFileInfo( Arg.AnyString ) )
				.Returns( ( IFileInfo ) null );

			// Ignore FileSystem.CopyFile calls by default.
			Mock.Arrange( () => FileSystem.CopyFile( Arg.AnyString, Arg.AnyString ) );

			// For any artifact hash we don't have, return null from ArtifactCache.FindArtifact.
			Mock.Arrange( () => ArtifactCache.FindArtifact( Arg.IsAny< IHash >() ) )
				.Returns( ( IArtifact ) null );

			// Return dummy artifacts for calls to ArtifactCache.CreateArtifact.
			Mock.Arrange( () => ArtifactCache.CreateArtifact( Arg.IsAny< IHash >() ) )
				.Returns( ( IHash hash ) => NewMock< IArtifact >().WithHash( hash ).WithPath( hash != null ? hash.ToString() : "" ) );
		}

        /// <summary>
        /// Make sure we don't accept not having inputs.
        /// </summary>
		[ Test ]
		public void Build_ThrowsExceptionForNullInput()
		{
			Assert.Throws< ArgumentException >( () => BuildEngine.Build( null ) );
		}

		/// <summary>
		/// Make sure the build engine accepts build with nothing in it.
		/// </summary>
		[ Test ]
		public void Build_DoesNothingIfInputHasNoBuildSteps()
		{
			// Arrange.
			var buildInputs = NewMock< IBuildInputs >().WithoutBuildSteps();

			// Act.
			Assert.DoesNotThrow( () => BuildEngine.Build( buildInputs ) );
		}

		/// <summary>
		/// Make sure the build engine runs a tool execution that has no input or output files.
		/// </summary>
		[ Test ]
		public void Build_RunsToolExecutionThatHasNoInputOrOutputFiles()
		{
			// Arrange.
			var toolExecution = NewMock< IToolExecution >()
				.WithoutInputFiles()
				.WithoutOutputFiles()
                .MustBeExecuted()
                .MustBeFoundUnstable();

			// Act.
			BuildEngine.Build( toolExecution );

			// Assert.
			Mock.Assert( toolExecution );
		}

        /// <summary>
        /// Make sure that if the output of a tool execution doesn't exist, we run it.
        /// </summary>
		[ Test ]
		public void Build_RunsToolExecutionWhereOutputFileDoesNotExist()
		{
			// Arrange.
			FileSystem.HasFile( "input" );
			
			var toolExecution = NewMock< IToolExecution >()
				.WithInputFiles( NewFileWithPath( "input" ) )
				.WithOutputFiles( NewFileWithPath( "output" ) )
				.MustBeExecuted()
				.MustBeFoundUnstable();

			// Act.
			BuildEngine.Build( toolExecution );

			// Assert.
			Mock.Assert( toolExecution );
		}

        /// <summary>
        /// Make sure that if the output file of a tool execution is older than the input file,
        /// we run it.
        /// </summary>
		[ Test ]
		public void Build_RunsToolExecutionWhereOutputFileIsOlderThanInputFile()
		{
            // Arrange.
            FileSystem.HasFile( "input" )
                .WithLastWriteTime( DateTime.Now );

            FileSystem.HasFile( "output" )
                .WithLastWriteTime( DateTime.Now - 10.Minutes() );

            var toolExecution = NewMock< IToolExecution >()
                .WithInputFiles( NewFileWithPath( "input" ) )
                .WithOutputFiles( NewFileWithPath( "output" ) )
                .MustBeExecuted()
                .MustBeFoundUnstable();

            // Act.
            BuildEngine.Build( toolExecution );

            // Assert.
            Mock.Assert( toolExecution );
		}

        /// <summary>
        /// Make sure that if the output file of a tool execution is never than the input
        /// file, we don't run it.
        /// </summary>
		[ Test ]
		public void Build_DoesNotRunToolExecutionWhereOutputFileIsNewerThanInputFile()
		{
			// Arrange.
            FileSystem.HasFile( "input" )
                .WithLastWriteTime( DateTime.Now - 10.Minutes() );

			FileSystem.HasFile( "include" )
				.WithLastWriteTime( DateTime.Now - 5.Minutes() );
            
            FileSystem.HasFile( "output" )
                .WithLastWriteTime( DateTime.Now );

            var toolExecution = NewMock< IToolExecution >()
                .WithInputFiles( NewFileWithPath( "input" ).WithIncludes( NewFileWithPath( "include" ) ) )
                .WithOutputFiles( NewFileWithPath( "output" ) )
				.MustComplete()
                .MustNotBeExecuted()
                .MustBeFoundStable();

            // Act.
            BuildEngine.Build( toolExecution );

            // Assert.
			Mock.Assert( toolExecution );
        }

		/// <summary>
		/// Make sure that a tool execution where only an include has changed is being run
		/// correctly.  Uses an indirect include to make sure we properly check includes
		/// of includes.
		/// </summary>
		[ Test ]
		public void Build_RunsToolExecutionIfIncludeFileHasChanged()
		{
			// Arrange.
			FileSystem.HasFile( "input" )
				.WithLastWriteTime( DateTime.Now - 20.Minutes() );

			FileSystem.HasFile( "include" )
				.WithLastWriteTime( DateTime.Now - 15.Minutes() );

			FileSystem.HasFile( "includeOfInclude" )
				.WithLastWriteTime( DateTime.Now );

			FileSystem.HasFile( "output" )
				.WithLastWriteTime( DateTime.Now - 10.Minutes() );

			var toolExecution = NewMock< IToolExecution >()
				.WithInputFiles(
					NewFileWithPath( "input" )
						.WithIncludes(
							NewFileWithPath( "include" )
								.WithIncludes( NewFileWithPath( "includeOfInclude") ) ) )
				.WithOutputFiles( NewFileWithPath( "output" ) )
				.MustBeExecuted()
				.MustBeFoundUnstable();

			// Act.
			BuildEngine.Build( toolExecution );

			// Assert.
			Mock.Assert( toolExecution );
		}

		/// <summary>
		/// Make sure that if an output is available from the artifact cache, the tool
		/// execution to produce it is not run.
		/// </summary>
		[ Test ]
		public void Build_TakesOutputFromArtifactCacheIfAvailable()
		{
			// Arrange.
			FileSystem.HasFile( "input" )
				.WithLastWriteTime( DateTime.Now - 10.Minutes() );

			var hash = NewMock< IHash >();
			var artifact = NewArtifact( hash );

			ArtifactCache.HasArtifact( artifact );

			var toolExecution = NewMock< IToolExecution >()
				.WithInputFiles( NewFileWithPath( "input" ) )
				.WithOutputFiles( NewFileWithPath( "output" ).WithHash( hash ) )
				.MustNotBeExecuted()
				.MustBeFoundUnstable()
				.MustComplete();

			Mock.Arrange( () => FileSystem.CopyFile( artifact.Path, "output" ) ).MustBeCalled();

			// Act.
			BuildEngine.Build( toolExecution );

			// Assert.
			Mock.Assert( toolExecution );
			Mock.Assert( FileSystem );
		}

		/// <summary>
		/// Make sure that the build system places puts generated files in the artifact
		/// cache, except they are marked as DontCache.
		/// </summary>
		[ Test ]
		public void Build_StoresOutputsInArtifactCache()
		{
			// Arrange.
			var hash1 = NewMock< IHash >();
			var hash2 = NewMock< IHash >();

			var artifact = NewMock< IArtifact >()
				.WithHash( hash1 )
				.WithPath( hash1.ToString() );

			var toolExecution = NewMock< IToolExecution >()
				.WithoutInputFiles()
				.WithOutputFiles
					(
						  NewFileWithPath( "output1" ).WithHash( hash1 )
						, NewFileWithPath( "output2" ).WithHash( hash2 ).DontCache()
					)
				.MustBeExecuted()
				.MustBeFoundUnstable();

			ArtifactCache.MustCreateArtifact( artifact );
			ArtifactCache.MustNotCreateArtifactWithHash( hash2 );

			// Act.
			BuildEngine.Build( toolExecution );
			
			// Assert.
			Mock.AssertAll( artifact );
			Mock.Assert( ArtifactCache );
		}

		/// <summary>
        /// Make sure that tools are run in the order of their build phases.
        /// </summary>
        [ Test ]
        public void Build_CompletesOneStepAfterTheOther()
        {
            // Arrange.
			var buildStep1 = NewMock< IBuildStep >()
				.WithoutToolExecutions()
				.MustComplete();
			var buildStep2 = NewMock< IBuildStep >()
				.WithoutToolExecutions()
				.MustCompleteAfter( buildStep1 );

            var buildInputs = NewMock< IBuildInputs >()
				.WithBuildSteps
                (
                      buildStep1
                    , buildStep2
                );

            // Act.
            BuildEngine.Build( buildInputs );

            // Assert.
            Mock.Assert( buildInputs );
        }

		/// <summary>
		/// Make sure the build engine expects all output paths to be fully set up
		/// in the build inputs.
		/// </summary>
		[ Test ]
		public void Build_ThrowsIfOutputFileHasNotBeenAssignedAPath()
		{
			// Arrange.
			FileSystem.HasAnyFile();
			var input = NewFileWithPath( "DoesNotMatter" );
			var output = NewMock< IFile >()
				.WithPath( null );

			var toolExecution = NewMock< IToolExecution >()
				.WithInputFiles( input )
				.WithOutputFiles( output )
				.MustNotBeExecuted();

			// Act.
			Action action = () => BuildEngine.Build( toolExecution );

			// Assert.
			action.ShouldThrow< InvalidOperationException >();
		}

		#endregion

        #region Non-Public Properties

        /// <summary>
		/// <see cref="BuildEngine"/> object we are testing.
		/// </summary>
        private BuildEngine BuildEngine { get; set; }

		/// <summary>
		/// Mock file system where we arrange files used by the build engine.
		/// </summary>
		private IFileSystem FileSystem { get; set; }

		/// <summary>
		/// Mock artifact cache where we monitor the cache activity generated by the
		/// build engine.
		/// </summary>
		private IArtifactCache ArtifactCache { get; set; }

		#endregion
	}
}
