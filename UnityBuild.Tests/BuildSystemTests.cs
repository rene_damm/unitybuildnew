﻿using System;
using NUnit.Framework;
using Telerik.JustMock;
using UnityBuild.Builds;
using UnityBuild.Files;
using UnityBuild.Targets;
using UnityBuild.Tests.Helpers;
using UnityBuild.Tools;
using UnityBuild.Utilities;

namespace UnityBuild.Tests
{
	/// <summary>
	/// Unit tests for <see cref="BuildSystem"/>.
	/// </summary>
	public class BuildSystemTests
		: UnitTestFixture
	{
		#region Public Methods

		[ SetUp ]
		public void SetUp()
		{
			BuildEngine = NewMock< IBuildEngine >();
			FileSystem = NewMock< IFileSystem >();
			TargetCollection = NewMock< ITargetCollection >();

			TargetCollectionConstructor = NewMock< Func< IBuildFiles, ITargetCollection > >();
			Mock.Arrange( () => TargetCollectionConstructor( Arg.IsAny< IBuildFiles >() ) )
				.Returns( TargetCollection )
				.OccursOnce();

			FileCollectionConstructor = NewMock< Func< string, IFileCollection > >();
			Mock.Arrange( () => FileCollectionConstructor( Arg.AnyString ) )
				.Returns( NewMock< IFileCollection >() );

			TargetSettingsConstructor = NewMock< Func< ITargetSettings > >()
				.Returns( NewMock< ITargetSettings >() );

			BuildSystem = new BuildSystem(
				  BuildEngine
				, FileSystem
				, FileCollectionConstructor
				, TargetCollectionConstructor
				, TargetSettingsConstructor
			);
		}

		/// <summary>
		/// Make sure the build system will create the right target and then build it.
		/// </summary>
		[ Test ]
		public void RunBuild_CreatesTargetAndBuildsIt()
		{
			// Arrange.
			const string targetName = "target";
			var target = NewStub< ITarget >();
			var targetSettings = NewStub< ITargetSettings >();
			TargetSettingsConstructor.Returns( targetSettings );

            Mock.Arrange( () => TargetCollection.FindOrAddTargetByName( targetName, targetSettings ) )
                .Returns( target )
                .MustBeCalled();
            Mock.Arrange( () => BuildEngine.Build( Arg.IsAny< IBuildInputs >() ) )
                .DoNothing()
                .MustBeCalled();

			// Act.
			BuildSystem.RunBuild( targetName );

			// Assert.
			Mock.Assert( BuildEngine );
			Mock.Assert( TargetCollection );
		}

		/// <summary>
		/// Make sure that the build system sets up target settings based on the
		/// textual arguments passed to it.
		/// </summary>
		[ Test ]
		public void RunBuild_ConfiguresSettingsFromArguments()
		{
			// Arrange.
			TargetCollection.CreatesAnyTarget();
			BuildEngine.IgnoresAnyBuild();

			var targetSettings = Mock.Create< TestTargetSettings >( Constructor.NotMocked, Behavior.Strict );
			Mock.ArrangeSet( () => targetSettings.TestOption = true )
				.MustBeCalled();
			TargetSettingsConstructor
				.Returns( targetSettings );

			// Act.
			BuildSystem.RunBuild( "target", "TestOption=1" );

			// Assert.
			Mock.Assert( targetSettings );
		}

		/// <summary>
		/// Make sure we don't accept not having a target name.
		/// </summary>
		[ Test ]
		public void RunBuild_ThrowsIfTargetNameIsNullOrEmpty()
		{
			Assert.Throws< ArgumentException >( () => BuildSystem.RunBuild( null ) );
			Assert.Throws< ArgumentException >( () => BuildSystem.RunBuild( "" ) );
		}

		#endregion

		#region Non-Public Properties

		/// <summary>
		/// Build system under test.
		/// </summary>
		private BuildSystem BuildSystem { get; set; }

		/// <summary>
		/// Mock build engine we feed to the build system.
		/// </summary>
		private IBuildEngine BuildEngine { get; set; }

		/// <summary>
		/// Mock file system we feed to the build system.
		/// </summary>
		private IFileSystem FileSystem { get; set; }

		/// <summary>
		/// Mock target collection we feed to the build system.
		/// </summary>
		private ITargetCollection TargetCollection { get; set; }

		private Func< IBuildFiles, ITargetCollection > TargetCollectionConstructor { get; set; }

		private Func< string, IFileCollection > FileCollectionConstructor { get; set; }

		private Func< ITargetSettings > TargetSettingsConstructor { get; set; }

		#endregion

		#region Inner Types

		public class TestTargetSettings
			: TargetSettings
		{
			#region Constructors

			public TestTargetSettings()
				: base( NewMock< IToolChain >() )
			{
			}

			#endregion

			#region Public Properties

			[ Option ]
			public virtual bool TestOption { get; set; }

			#endregion
		}

		#endregion
	}
}