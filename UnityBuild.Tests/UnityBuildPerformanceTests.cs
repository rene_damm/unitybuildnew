﻿using NUnit.Framework;
using UnityBuild.Tests.Helpers;

namespace UnityBuild.Tests
{
	public class UnityBuildPerformanceTests
		: PerformanceTestFixture
	{
		#region Public Methods

		/// <summary>
		/// Make sure that when we run a WindowsEditor build when all outputs are already
		/// fully up to date takes less than 3 seconds.
		/// </summary>
		[ Test ]
		[ Platform( "Win" ) ]
		public void EmptyWindowsEditorBuildTakesLessThanThreeSeconds()
		{
		}

		#endregion
	}
}