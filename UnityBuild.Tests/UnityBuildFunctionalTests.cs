﻿using System;
using System.Diagnostics;
using System.IO;
using FluentAssertions;
using NUnit.Framework;
using UnityBuild.Tests.Helpers;

namespace UnityBuild.Tests
{
	/// <summary>
	/// High-level tests that treat the the entire build system as a black box and make sure it
	/// can perform the high-level operations we can invoke via the runner interface.
	/// </summary>
	public class UnityBuildFunctionalTests
		: FunctionalTestFixture
	{
		#region Public Methods

		/// <summary>
		/// Make sure we can successfully build and run "build/WindowsEditor/Unity.exe".
		/// </summary>
		[ TestCase( "debug" ) ]
		[ TestCase( "release" ) ]
		[ Platform( "Win" ) ]
		public void CanBuildWindowsEditor( string configuration )
		{
			// Arrange.
			if ( Directory.Exists( BuildOutputPath ) )
				Directory.Delete( BuildOutputPath, true );
			var runner = new UnityBuildRunner( WorkingCopyPath );

			// Act.
			var result = runner.Run( "WindowsEditor", configuration );

			// Assert.
			result.Should().Be( 0 );

			var windowsEditorExecutablePath = Path.Combine( WorkingCopyPath, @"build\WindowsEditor\Unity.exe" );
			File.Exists( windowsEditorExecutablePath )
				.Should()
				.BeTrue();

			Action runEditor = () => Process.Start( windowsEditorExecutablePath, "-batchmode -quit" ).WaitForExit();
			runEditor
				.ShouldNotThrow();
		}

		#endregion
	}
}
