﻿using System;
using System.Collections.Generic;
using FluentAssertions;
using NUnit.Framework;
using UnityBuild.Tests.Helpers;
using UnityBuild.Utilities;

namespace UnityBuild.Tests.Utilities
{
	/// <summary>
	/// Unit tests for <see cref="TypeToInstanceTable{TBaseType}"/>.
	/// </summary>
	public class TypeToInstanceTableTests
		: UnitTestFixture
	{
		#region Public Methods

		[ SetUp ]
		public void SetUp()
		{
			TypeToInstanceTable = new TypeToInstanceTable< BaseClass >();
		}

		/// <summary>
		/// Make sure Add() won't accept not being given an instance.
		/// </summary>
		[ Test ]
		public void Add_ThrowsIfNotGivenAnInstance()
		{
			Assert.Throws< ArgumentNullException >( () => TypeToInstanceTable.Add( null ) );
		}

		/// <summary>
		/// Make sure Add() adds the instance we give it to the table.
		/// </summary>
		[ Test ]
		public void Add_AddsInstanceToTable()
		{
			// Arrange.
			var instance1 = new DerivedClassA();
			var instance2 = new DerivedClassB();

			// Act.
			TypeToInstanceTable.Add( instance1 );
			TypeToInstanceTable.Add( instance2  );

			// Assert.
			TypeToInstanceTable.Values.Should().Contain( instance1 );
			TypeToInstanceTable.Values.Should().Contain( instance2 );
		}

		/// <summary>
		/// Make sure Add() replaces existing mappings for the same instance type.
		/// </summary>
		[ Test ]
		public void Add_ReplacesExistingInstancesOfSameType()
		{
			// Arrange.
			var instance1 = new DerivedClassA();
			var instance2 = new DerivedClassA();

			// Act.
			TypeToInstanceTable.Add( instance1 );
			TypeToInstanceTable.Add( instance2 );

			// Assert.
			TypeToInstanceTable.Values.Should().NotContain( instance1 );
			TypeToInstanceTable.Values.Should().Contain( instance2 );
		}

		/// <summary>
		/// Make sure Get() finds an instance we previously entered into the table.
		/// </summary>
		[ Test ]
		public void Get_ReturnsInstanceWithMatchingType()
		{
			// Arrange.
			var instance = new DerivedClassA();
			TypeToInstanceTable.Add( instance );

			// Act.
			var result = TypeToInstanceTable.Get< DerivedClassA >();

			// Assert.
			result.Should().BeSameAs( instance );
		}

		/// <summary>
		/// Make sure Get() will return an instance that we query from one of its base
		/// types rather than the actual instance type.
		/// </summary>
		[ Test ]
		public void Get_RespectsSubtyping()
		{
			// Arrange.
			var instance = new DerivedClassA();
			TypeToInstanceTable.Add( instance );	

			// Act.
			var result = TypeToInstanceTable.Get< BaseClass >();

			// Assert.
			result.Should().BeSameAs( instance );
		}

		/// <summary>
		/// Make sure Get() doesn't return null but rather throws if we ask it for
		/// an instance of a type it doesn't know.
		/// </summary>
		[ Test ]
		public void Get_ThrowsIfTypeNotKnown()
		{
			Assert.Throws< KeyNotFoundException >( () => TypeToInstanceTable.Get< DerivedClassA >() );
		}

		#endregion

		#region Non-Public Properties

		private TypeToInstanceTable< BaseClass > TypeToInstanceTable { get; set; }

		#endregion

		#region Inner Types

		private abstract class BaseClass
		{
		}

		private class DerivedClassA
			: BaseClass
		{
		}

		private class DerivedClassB
			: BaseClass
		{
		}

		#endregion
	}
}