﻿using System.Text;
using NUnit.Framework;
using UnityBuild.Tests.Helpers;
using UnityBuild.Utilities;

namespace UnityBuild.Tests.Utilities
{
	/// <summary>
	/// Unit tests for <see cref="Md5HashAlgorithm"/>.
	/// </summary>
	public class Md5HashAlgorithmTests
		: UnitTestFixture
	{
		#region Public Methods

		/// <summary>
		/// Make sure generating a hash from a byte array returns a correct MD5 hash.
		/// </summary>
		[ Test ]
		public void GenerateHash_FromByteArray_ReturnsCorrectHash()
		{
			var result = new Md5HashAlgorithm().GenerateHash( TestTextData );
			Assert.That( result.ToString(), Is.EqualTo( TestHash ) );
		}

		/// <summary>
		/// Make sure generating a hash from a stream returns a correct MD5 hash.
		/// </summary>
		[ Test ]
		public void GenerateHash_FromStream_ReturnsCorrectHash()
		{
			var result = new Md5HashAlgorithm().GenerateHash( TestTextData.ToStream() );
			Assert.That( result.ToString(), Is.EqualTo( TestHash ) );
		}

		#endregion

		#region Non-Public Constants

		private const string TestText = "This is a test string.";

		private const string TestHash = "1620d7b066531f9dbad51eee623f7635";

		#endregion

		#region Non-Public Properties

		private byte[] TestTextData
		{
			get { return Encoding.ASCII.GetBytes( TestText ); }
		}

		#endregion
	}
}