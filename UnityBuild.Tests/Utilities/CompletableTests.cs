﻿using System;
using NUnit.Framework;
using UnityBuild.Tests.Helpers;
using UnityBuild.Utilities;

namespace UnityBuild.Tests.Utilities
{
	public class CompletableTests
		: UnitTestFixture
	{
		#region Public Methods

		[ SetUp ]
		public void SetUp()
		{
			Completable = new TestCompletable();
		}

		#endregion

		#region Public Methods

		/// <summary>
		/// Make sure we start out being incomplete.
		/// </summary>
		[ Test ]
		public void IsComplete_DefaultsToFalse()
		{
			Assert.That( Completable.IsComplete, Is.False );
		}

		/// <summary>
		/// Make sure we signal <see cref="ICompletable.OnComplete"/> when target is completed.
		/// </summary>
		[ Test ]
		public void IsComplete_SignalsOnCompleteIfSetToTrue()
		{
			// Arrange.
			var onCompleteRaised = false;
			Completable.OnComplete += _ => onCompleteRaised = true;

			// Act.
			Completable.IsComplete = true;

			// Assert.
			Assert.That( onCompleteRaised, Is.True );
		}

		/// <summary>
		/// Make sure we don't allow setting <see cref="ICompletable.IsComplete"/> back to false
		/// once it has been set to true.
		/// </summary>
		[ Test ]
		public void IsComplete_ThrowsIfTryingToRevertToIncomplete()
		{
			Completable.IsComplete = true;
			Assert.Throws< InvalidOperationException >( () => Completable.IsComplete = false );
		}

		#endregion

		#region Non-Public Properties

		private Completable Completable { get; set; }

		#endregion

		#region Inner Types

		private class TestCompletable
			: Completable
		{
		}

		#endregion
	}
}
