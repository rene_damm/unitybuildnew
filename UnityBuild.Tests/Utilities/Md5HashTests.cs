﻿using System;
using NUnit.Framework;
using UnityBuild.Tests.Helpers;
using UnityBuild.Utilities;

namespace UnityBuild.Tests.Utilities
{
	/// <summary>
	/// Unit tests for <see cref="Md5Hash"/>.
	/// </summary>
	public class Md5HashTests
		: UnitTestFixture
	{
		#region Public Methods

		/// <summary>
		/// Make sure we only accept hash data that is exactly 16 bytes long.
		/// </summary>
		[ Test ]
		public void Constructor_ThrowsIfDataIsNot16Bytes()
		{
			Assert.Throws< ArgumentException >( () => new Md5Hash( new byte[] { 1, 2, 3 } ) );
			Assert.DoesNotThrow( () => new Md5Hash( new byte[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16 } ) );
		}

		#endregion
	}
}