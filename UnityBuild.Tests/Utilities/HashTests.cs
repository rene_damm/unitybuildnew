﻿using System;
using NUnit.Framework;
using UnityBuild.Tests.Helpers;
using UnityBuild.Utilities;

namespace UnityBuild.Tests.Utilities
{
	/// <summary>
	/// Unit tests for <see cref="Hash"/>.
	/// </summary>
	public class HashTests
		: UnitTestFixture
	{
		#region Public Methods

		/// <summary>
		/// Make sure the constructor doesn't accept for there to be no hash data.
		/// </summary>
		[ Test ]
		public void Constructor_ThrowsIfNoData()
		{
			Assert.Throws< ArgumentException >( () => new TestHash( null ) );
			Assert.Throws< ArgumentException >( () => new TestHash( new byte[] {} ) );
		}

		/// <summary>
		/// Make sure ToString() converts the raw hash data into a proper hex digit sequence.
		/// </summary>
		[ Test ]
		public void ToString_ReturnsHexSequenceOfData()
		{
			var data = new byte[] { 0xDE, 0xAD, 0xBE, 0xEF, 0x12 };
			const string text = "deadbeef12";

			Assert.That( new TestHash( data ).ToString(), Is.EqualTo( text ) );
		}

		#endregion

		#region Inner Types

		private class TestHash
			: Hash
		{
			#region Constructors

			public TestHash( byte[] rawData )
				: base( rawData )
			{
			}

			#endregion
		}

		#endregion
	}
}