﻿using NUnit.Framework;
using Telerik.JustMock;
using UnityBuild.Tests.Helpers;
using UnityBuild.Utilities;

namespace UnityBuild.Tests.Utilities
{
	/// <summary>
	/// Unit tests for <see cref="OptionParser"/>.
	/// </summary>
	public class OptionParserTests
		: UnitTestFixture
	{
		#region Public Methods

		[ SetUp ]
		public void SetUp()
		{
			Instance = Mock.Create< ClassWithOptionProperties >( Constructor.NotMocked, Behavior.Strict );
		}

		/// <summary>
		/// Make sure we lower-case everything before comparing.
		/// </summary>
		[ Test ]
		public void ParseOptions_IgnoresCase()
		{
			// Arrange.
			Mock.ArrangeSet( () => Instance.StringValue = "test" )
				.MustBeCalled();

			// Act.
			OptionParser.ParseOptions( Instance, "StrINg=test" );

			// Assert.
			Mock.Assert( Instance );
		}

		/// <summary>
		/// Make sure we ignore leading dashes (however many there are).
		/// </summary>
		[ Test ]
		public void ParseOptions_StripsDashes()
		{
			// Arrange.
			Mock.ArrangeSet( () => Instance.StringValue = "value" )
				.MustBeCalled();

			// Act.
			OptionParser.ParseOptions( Instance, "--string=value" );

			// Assert.
			Mock.Assert( Instance );
		}

		[ Test ]
		public void ParseOptions_RequiresArgumentForStringOption()
		{
			////TODO
		}

		/// <summary>
		/// Make sure we take the mentioning of a bool option without a value to
		/// mean that the value is to be set to true.
		/// </summary>
		[ Test ]
		public void ParseOptions_DoesNotRequireValueForBoolean()
		{
			// Arrange.
			Mock.ArrangeSet( () => Instance.BooleanValue = true )
				.MustBeCalled();

			// Act.
			OptionParser.ParseOptions( Instance, "bool" );

			// Assert.
			Mock.Assert( Instance );
		}

		/// <summary>
		/// Make sure we recognize "1" as true.
		/// </summary>
		[ Test ]
		public void ParseOptions_RecognizesNumericOneAsTrue()
		{
			// Arrange.
			Mock.ArrangeSet( () => Instance.BooleanValue = true )
				.MustBeCalled();

			// Act.
			OptionParser.ParseOptions( Instance, "bool=1" );

			// Assert.
			Mock.Assert( Instance );
		}

		/// <summary>
		/// Make sure we recognize "0" as false.
		/// </summary>
		[ Test ]
		public void ParseOptions_RecognizesNumericZeroAsFalse()
		{
			// Arrange.
			Mock.ArrangeSet( () => Instance.BooleanValue = false )
				.MustBeCalled();

			// Act.
			OptionParser.ParseOptions( Instance, "bool=0" );

			// Assert.
			Mock.Assert( Instance );
		}

		/// <summary>
		/// Make sure we recognize "true" as true.
		/// </summary>
		[ Test ]
		public void ParseOptions_RecognizesTrueAsTrue()
		{
			// Arrange.
			Mock.ArrangeSet( () => Instance.BooleanValue = true )
				.MustBeCalled();

			// Act.
			OptionParser.ParseOptions( Instance, "bool=true" );

			// Assert.
			Mock.Assert( Instance );
		}

		/// <summary>
		/// Make sure we recognize "false" as false.
		/// </summary>
		[ Test ]
		public void ParseOptions_RecognizesFalseAsFalse()
		{
			// Arrange.
			Mock.ArrangeSet( () => Instance.BooleanValue = false )
				.MustBeCalled();

			// Act.
			OptionParser.ParseOptions( Instance, "bool=false" );

			// Assert.
			Mock.Assert( Instance );
		}

		/// <summary>
		/// Make sure we recognize "on" as true.
		/// </summary>
		[ Test ]
		public void ParseOptions_RecognizesOnAsTrue()
		{
			// Arrange.
			Mock.ArrangeSet( () => Instance.BooleanValue = true )
				.MustBeCalled();

			// Act.
			OptionParser.ParseOptions( Instance, "bool=on" );

			// Assert.
			Mock.Assert( Instance );
		}

		/// <summary>
		/// Make sure we recognize "off" as true.
		/// </summary>
		[ Test ]
		public void ParseOptions_RecognizesOffAsTrue()
		{
			// Arrange.
			Mock.ArrangeSet( () => Instance.BooleanValue = false )
				.MustBeCalled();

			// Act.
			OptionParser.ParseOptions( Instance, "bool=off" );

			// Assert.
			Mock.Assert( Instance );
		}

		[ Test ]
		public void ParseOptions_DefaultsToPropertyNameAsOptionName()
		{
			// Arrange.
			Mock.ArrangeSet( () => Instance.OptionWithoutExplicitName = true )
				.MustBeCalled();

			// Act.
			OptionParser.ParseOptions( Instance, "OptionWithoutExplicitName" );

			// Assert.
			Mock.Assert( Instance );
		}

		#endregion

		#region Non-Public Properties

		private ClassWithOptionProperties Instance { get; set; }

		#endregion

		#region Inner Types

		public class ClassWithOptionProperties
		{
			#region Public Properties

			[ Option( "bool" ) ]
			public virtual bool BooleanValue { get; set; }

			[ Option( "strING" ) ]
			public virtual string StringValue { get; set; }

			[ Option ]
			public virtual bool OptionWithoutExplicitName { get; set; }

			#endregion
		}

		#endregion
	}
}