﻿using System;
using NUnit.Framework;
using UnityBuild.Tests.Helpers;
using UnityBuild.Utilities;

namespace UnityBuild.Tests.Utilities
{
	/// <summary>
	/// Unit tests for <see cref="StringExtensions"/>.
	/// </summary>
	public class StringExtensionsTests
		: UnitTestFixture
	{
		#region Public Methods

		/// <summary>
		/// Make sure ToByteArray() returns the raw Unicode byte sequence.
		/// </summary>
		[ Test ]
		public void ToByteArray_ReturnsUnicodeByteSequence()
		{
			// Arrange.
			const string text = "ABC1 Ɍ";
			
			var bytes = BitConverter.IsLittleEndian
				? new byte[] { 0x41, 0x00, 0x42, 0x00, 0x43, 0x00, 0x31, 0x00, 0x20, 0x00, 0x4C, 0x02 }
				: new byte[] { 0x00, 0x41, 0x00, 0x42, 0x00, 0x43, 0x00, 0x31, 0x00, 0x20, 0x02, 0x4C };

			// Act.
			var result = text.ToByteArray();

			// Assert.
			Assert.That( result, Is.EqualTo( bytes ) );
		}

		#endregion
	}
}