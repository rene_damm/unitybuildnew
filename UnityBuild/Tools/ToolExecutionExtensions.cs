﻿using System.Collections.Generic;

namespace UnityBuild.Tools
{
	/// <summary>
	/// Extension methods to simplify working with tool executions.
	/// </summary>
	public static class ToolExecutionExtensions
	{
		#region Public Methods

		public static IEnumerable< ToolExecution > WithToolSettings( this IEnumerable< ToolExecution  > toolExecutions, IToolSettings toolSettings )
		{
			foreach ( var execution in toolExecutions )
				execution.ToolSettings = toolSettings;
			return toolExecutions;
		}

		public static IEnumerable< ToolExecution > WithTool( this IEnumerable< ToolExecution  > toolExecutions, ITool tool )
		{
			foreach ( var execution in toolExecutions )
				execution.Tool = tool;
			return toolExecutions;
		}

		#endregion
	}
}