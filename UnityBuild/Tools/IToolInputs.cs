﻿using System.Collections.Generic;
using UnityBuild.Files;

namespace UnityBuild.Tools
{
	public interface IToolInputs
	{
		/// <summary>
		/// Settings to be used by the tool.
		/// </summary>
		IToolSettings ToolSettings { get; }

		IEnumerable< IFile > InputFiles { get; }

		IEnumerable< IFile > OutputFiles { get; }

		bool? Stable { get; set; }
	}
}
