﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using UnityBuild.Utilities;

namespace UnityBuild.Tools
{
	/// <summary>
	/// Base class for tools that are launched as external processes with configuration passed
	/// through the command line.
	/// </summary>
	public abstract class CommandLineTool
		: Tool< CommandLineToolSettings >
	{
		#region Constructors

		protected CommandLineTool( IProcessFactory processFactory )
		{
			if ( processFactory == null )
				throw new ArgumentException( "Process factory cannot be null!", "processFactory" );

			ProcessFactory = processFactory;
		}

		#endregion

		#region Non-Public Methods

		protected override void InternalRun( IToolInputs inputs, CommandLineToolSettings settings )
		{
			// Determine executable to run.
			var executablePath = GetExecutablePath( inputs );
			if ( string.IsNullOrEmpty( executablePath ) )
			{
				executablePath = ExecutablePath;
				if ( string.IsNullOrEmpty( executablePath ) )
					throw new InvalidOperationException( "ExecutablePath of command line tool has not been set!" );
			}
			Debug.Assert( !string.IsNullOrEmpty( executablePath ) );

			// Create start info.
			var startInfo =
				new ProcessStartInfo
			        {
				        FileName = ExecutablePath
			        };

			// Spawn process.
			var process = ProcessFactory.NewProcess( startInfo );

			////TODO: wait
		}

		protected virtual string GetExecutablePath( IToolInputs inputs )
		{
			return null;
		}

		protected virtual void AddArguments( IToolInputs inputs, StringBuilder buffer )
		{
		}

		protected virtual void AddEnvironmentVariables( IToolInputs inputs, IDictionary< string, string > environment )
		{
			////TODO: add from EnvironmentVariables property
		}

		#endregion

		#region Public Properties

		public IProcessFactory ProcessFactory { get; private set; }

		public string ExecutablePath { get; set; }

		public IDictionary< string, string > EnvironmentVariables
		{
			get
			{
				if ( _environmentVariables == null )
					_environmentVariables = new Dictionary< string, string >();

				return _environmentVariables;
			}

			set { _environmentVariables = value; }
		}

		#endregion

		#region Fields

		private IDictionary< string, string > _environmentVariables;

		#endregion
	}
}