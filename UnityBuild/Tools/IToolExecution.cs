﻿using UnityBuild.Utilities;

namespace UnityBuild.Tools
{
	public interface IToolExecution
		: ICompletable
	{
		ITool Tool { get; }

		IToolInputs ToolInputs { get; }
	}
}