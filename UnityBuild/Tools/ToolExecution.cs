﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityBuild.Files;
using UnityBuild.Utilities;

namespace UnityBuild.Tools
{
	/// <summary>
	/// Aggregation of data to run a tool with specific inputs.
	/// </summary>
    public sealed class ToolExecution
        : Completable
        , IToolExecution
		, IToolInputs
    {
        #region Constructors

        public ToolExecution( ITool tool, IToolSettings toolSettings )
        {
            if ( tool == null )
                throw new ArgumentException( "Tool cannot be null!", "tool" );
            if ( toolSettings == null )
                throw new ArgumentException( "Tool settings cannot be null!", "toolSettings" );

            Tool = tool;
            ToolSettings = toolSettings;
        }

        #endregion

        #region Public Methods

        public void AddInputFile( IFile file )
        {
            if ( file == null )
                throw new ArgumentException( "File cannot be null!", "file" );

            if ( _multipleInputFiles != null )
                _multipleInputFiles.Add( file );
            else if ( _singleInputFile != null )
            {
                _multipleInputFiles = new List< IFile >
                {
                      _singleInputFile
                    , file
                };
                _singleInputFile = null;
            }
            else
                _singleInputFile = file;
        }

        public void AddOutputFile( IFile file )
        {
            if ( file == null )
                throw new ArgumentException( "File cannot be null!", "file" );

            if ( _multipleOutputFiles != null )
                _multipleOutputFiles.Add( file );
            else if ( _singleOutputFile != null )
            {
                _multipleOutputFiles = new List< IFile >
                {
                      _singleOutputFile
                    , file
                };
                _singleOutputFile = null;
            }
            else
                _singleOutputFile = file;
        }

		#endregion

		#region Public Properties

        public ITool Tool { get; set; }

	    IToolInputs IToolExecution.ToolInputs
	    {
		    get { return this; }
	    }

        public IToolSettings ToolSettings { get; set; }

        public IEnumerable< IFile > InputFiles
        {
            get
            {
                if ( _singleInputFile != null )
                    return SingleInputFileAsEnumerable;

                if ( _multipleInputFiles != null )
                    return _multipleInputFiles;
                
                return Enumerable.Empty< IFile >();
            }
        }

        public IEnumerable< IFile > OutputFiles
        {
            get
            {
                if ( _singleOutputFile != null )
                    return SingleOutputFileAsEnumerable;

                if ( _multipleOutputFiles != null )
                    return _multipleOutputFiles;
                
                return Enumerable.Empty< IFile >();
            }
        }

        public bool? Stable { get; set; }

		#endregion

        #region Non-Public Properties

        private IEnumerable< IFile > SingleInputFileAsEnumerable
        {
            get { yield return _singleInputFile; }
        }

        private IEnumerable< IFile > SingleOutputFileAsEnumerable
        {
            get { yield return _singleOutputFile; }
        }

        #endregion

        #region Fields

        private IFile _singleInputFile;
        private IFile _singleOutputFile;
        private List< IFile > _multipleInputFiles;
        private List< IFile > _multipleOutputFiles; 

        #endregion
    }
}
