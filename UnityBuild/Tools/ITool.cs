﻿namespace UnityBuild.Tools
{
    /// <summary>
    /// A tool is a piece of build logic that can be run on a given set of inputs.
    /// </summary>
	public interface ITool
	{
		void Run( IToolInputs inputs );
	}
}
