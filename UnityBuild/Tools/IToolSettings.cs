﻿using UnityBuild.Settings;
using UnityBuild.Utilities;

namespace UnityBuild.Tools
{
	public interface IToolSettings
        : IBuildSettings
        , IHashed
	{
	}
}