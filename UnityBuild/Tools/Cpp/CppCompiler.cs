﻿using UnityBuild.Utilities;

namespace UnityBuild.Tools.Cpp
{
	public abstract class CppCompiler
		: CommandLineTool
	{
		#region Constructors

		protected CppCompiler( IProcessFactory processFactory )
			: base( processFactory )
		{
		}

		#endregion
	}
}
