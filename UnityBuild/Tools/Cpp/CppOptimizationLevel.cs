﻿namespace UnityBuild.Tools.Cpp
{
	public enum CppOptimizationLevel
	{
		DontOptimize,
		OptimizeForSpeed,
		OptimizeForSize
	}
}