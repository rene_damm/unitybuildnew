﻿namespace UnityBuild.Tools.Cpp
{
	public class CppCompilerSettings
		: CommandLineToolSettings
	{
		#region Public Properties

		public bool GenerateDebugInfo { get; set; }

		public CppOptimizationLevel OptimizationLevel { get; set; }

		public bool TreatWarningsAsErrors { get; set; }

		#endregion
	}
}
