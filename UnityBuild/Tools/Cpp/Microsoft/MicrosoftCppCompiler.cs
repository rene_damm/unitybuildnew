﻿using UnityBuild.Utilities;

namespace UnityBuild.Tools.Cpp.Microsoft
{
	public class MicrosoftCppCompiler
		: CppCompiler
	{
		#region Constructors

		public MicrosoftCppCompiler( IProcessFactory processFactory )
			: base( processFactory )
		{
		}

		#endregion
	}
}
