﻿using UnityBuild.Utilities;

namespace UnityBuild.Tools.Cpp.Microsoft
{
	public class MicrosoftCppLinker
		: CppLinker
	{
		#region Constructors

		public MicrosoftCppLinker( IProcessFactory processFactory )
			: base( processFactory )
		{
		}

		#endregion
	}
}