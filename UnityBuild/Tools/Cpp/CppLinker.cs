﻿using UnityBuild.Utilities;

namespace UnityBuild.Tools.Cpp
{
	public abstract class CppLinker
		: CommandLineTool
	{
		#region Constructors

		protected CppLinker( IProcessFactory processFactory )
			: base( processFactory )
		{
		}

		#endregion
	}
}