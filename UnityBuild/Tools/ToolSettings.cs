﻿using UnityBuild.Utilities;

namespace UnityBuild.Tools
{
    public class ToolSettings
		: Settings.BuildSettings
		, IToolSettings
    {
	    #region Public Properties

	    public IHash Hash { get; private set; }

	    #endregion
    }
}