﻿using System.Collections.Generic;

namespace UnityBuild.Tools
{
	public class CommandLineToolSettings
		: ToolSettings
	{
		#region Public Properties

		public IList< string > ExtraArguments
		{
			get { return _extraArguments; }
		}

		#endregion

		#region Fields

		private readonly List< string > _extraArguments = new List< string >();

		#endregion
	}
}
