﻿using System;

namespace UnityBuild.Tools
{
	public abstract class Tool< TSettings >
		: ITool
		where TSettings : class, IToolSettings
	{
		#region Public Methods

		public void Run( IToolInputs inputs )
		{
			if ( inputs == null )
				throw new ArgumentException( "Inputs cannot be null!", "inputs" );
			
			var settings = inputs.ToolSettings;
			if ( inputs.ToolSettings == null )
				throw new ArgumentException( "Tool settings cannot be null!", "inputs" );

			var settingsImpl = inputs.ToolSettings as TSettings;
			if ( settingsImpl == null )
				throw new ArgumentException
					(
						  string.Format( "Invalid settings type '{0}'; expecting settings of type '{1}'!",
							settings.GetType(), typeof( TSettings ) )
						, "inputs"
					);

			InternalRun( inputs, settingsImpl );
		}

		#endregion

		#region Non-Public Methods

		protected abstract void InternalRun( IToolInputs inputs, TSettings settings );

		#endregion
	}
}
