﻿using UnityBuild.Utilities;

namespace UnityBuild.Tools
{
	/// <summary>
	/// A collection of tools and default settings for those tools.
	/// </summary>
	public interface IToolChain
		: INamed
	{
		TTool GetTool< TTool >()
			where TTool : ITool;

        TToolSettings GetToolSettings< TToolSettings >()
			where TToolSettings : IToolSettings;
	}
}
