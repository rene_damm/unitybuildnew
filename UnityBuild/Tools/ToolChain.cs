﻿using System;
using System.Collections.Generic;
using UnityBuild.Utilities;

namespace UnityBuild.Tools
{
	/// <summary>
	/// A modifiable collection of tools and tool settings.
	/// </summary>
	public class ToolChain
		: IToolChain
	{
		#region Constructors

		public ToolChain( IEnumerable< ITool > tools = null, IEnumerable< IToolSettings > toolSettings = null )
		{
			if ( tools != null )
			{
				foreach ( var tool in tools )
					AddTool( tool );				
			}

			if ( toolSettings != null )
			{
				foreach ( var settings in toolSettings )
					AddToolSettings( settings );				
			}
		}

		#endregion

		#region Public Methods

		public void AddTool( ITool tool )
		{
			_tools.Add( tool );
		}

		public void AddToolSettings( IToolSettings toolSettings )
		{
			_toolSettings.Add( toolSettings );
		}

		public TTool GetTool< TTool >()
			where TTool : ITool
		{
			try
			{
				return _tools.Get< TTool >();
			}
			catch ( KeyNotFoundException inner )
			{
				throw new KeyNotFoundException(
					  string.Format(
						  "The build is looking for a tool of type '{0}' but no instance of it was found in the toolchain '{1}'."
						, typeof( TTool )
						, this )
					, inner );
			}
		}

		public TToolSettings GetToolSettings< TToolSettings >() where TToolSettings : IToolSettings
		{
			try
			{
				return _toolSettings.Get< TToolSettings >();
			}
			catch ( KeyNotFoundException inner )
			{
				throw new KeyNotFoundException(
					  string.Format(
						  "The build is looking for tool settings of type '{0}' but no instance of it was found in the toolchain '{1}'."
						, typeof( TToolSettings )
						, this )
					, inner );
			}
		}

		#endregion

		#region Public Properties

		public string Name { get; private set; }

		public IReadOnlyDictionary< Type, ITool > Tools
		{
			get { return _tools; }
		}

		public IReadOnlyDictionary< Type, IToolSettings > ToolSettings
		{
			get { return _toolSettings; }
		}

		#endregion

		#region Fields

		private readonly TypeToInstanceTable< ITool > _tools = new TypeToInstanceTable< ITool >();
		private readonly TypeToInstanceTable< IToolSettings > _toolSettings = new TypeToInstanceTable< IToolSettings >();

		#endregion
	}
}