﻿using System;

namespace UnityBuild.Utilities
{
	public interface ICompletable
	{
		bool IsComplete { get; set; }

		event Action< ICompletable > OnComplete;
	}
}