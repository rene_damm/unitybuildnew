﻿using System;
using System.Collections.Generic;
using System.Reflection;

////TODO: allow to shorten option names automatically to the shortest possible unambiguous name

namespace UnityBuild.Utilities
{
	public static class OptionParser
	{
		#region Public Methods

		public static void ParseOptions( object configurable, params string[] args )
		{
			// Grab all the properties from the type that have
			// OptionAttributes.
			var configurablePropertyTable = CollectConfigurableProperties( configurable.GetType() );

			//
			foreach ( var argument in args )
			{
				// Skip empty strings.
				var totalNumChars = argument.Length;
				if ( totalNumChars == 0 )
					continue;

				// Skip leading dashes.
				var nameStartIndex = 0;
				while ( argument[ nameStartIndex ] == '-' )
					++ nameStartIndex;

				// Find the end of the option name part.
				var nameEndIndex = nameStartIndex + 1;
				while ( nameEndIndex < totalNumChars && argument[ nameEndIndex ] != '=' )
					++ nameEndIndex;

				// Extract the option name.  Slight optimization by
				// simply using the original string if there were neither
				// dashes nor an argument.
				var optionName =
					  ( nameStartIndex == 0 && nameEndIndex == totalNumChars )
					? argument
					: argument.Substring( nameStartIndex, nameEndIndex - nameStartIndex );

				// Lower-case the option name.
				optionName = optionName.ToLower();

				// Extract value string, if present.
				string valueString = null;
				if ( nameEndIndex < totalNumChars && argument[ nameEndIndex ] == '=' )
				{
					var valueStartIndex = nameEndIndex + 1;
					valueString = argument.Substring( valueStartIndex );
				}

				////TODO: handle the case properly where the argument does not refer to a valid option

				// Find property.
				var property = configurablePropertyTable[ optionName ];
				
				// Set property value.
				var propertyType = property.PropertyType;
				if ( propertyType == typeof( string ) )
				{
					property.SetValue( configurable, valueString );
				}
				else if ( propertyType == typeof( bool ) )
				{
					var value = true;
					if ( valueString != null )
					{
						valueString = valueString.ToLower();
						switch ( valueString )
						{
							case "on":      value = true; break;
							case "off":     value = false; break;
							case "true":    value = true; break;
							case "false":   value = false; break;
							case "1":       value = true; break;
							case "0":       value = false; break;
							default:
								throw new NotImplementedException();
						}
					}

					property.SetValue( configurable, value );
				}
				else
				{
					throw new NotImplementedException();
				}
			}
		}

		#endregion

		#region Non-Public Methods

		private static Dictionary< string, PropertyInfo > CollectConfigurableProperties( Type type )
		{
			var table = new Dictionary< string, PropertyInfo >();

			// Look at all the public properties of the type.
			foreach ( var property in type.GetProperties() )
			{
				// Check if this property is an option.
				var optionAttribute = property.GetCustomAttribute< OptionAttribute >();
				if ( optionAttribute == null )
				{
					// No, skip.
					continue;
				}

				// If we not given any explicit option names, default to the property
				// name as the only name for the option.
				var allNames = optionAttribute.Names;
				if ( allNames == null || allNames.Count == 0 )
					allNames = new List< string > { property.Name };

				// Add each option name mapping to the table.
				foreach ( var name in allNames )
				{
					////TODO: detect conflicts between option names
					table[ name.ToLower() ] = property;
				}
			}

			return table;
		}

		#endregion
	}
}