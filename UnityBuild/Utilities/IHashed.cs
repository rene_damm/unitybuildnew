﻿namespace UnityBuild.Utilities
{
	public interface IHashed
	{
		IHash Hash { get; }
	}
}