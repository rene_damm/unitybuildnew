﻿namespace UnityBuild.Utilities
{
	public interface IHashable
		: IHashed
	{
		new IHash Hash { get; set; }
	}
}