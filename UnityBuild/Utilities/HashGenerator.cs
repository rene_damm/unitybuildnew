﻿using System.IO;

namespace UnityBuild.Utilities
{
	public abstract class HashGenerator
		: BinaryWriter
	{
		public abstract IHash Finish();
	}
}