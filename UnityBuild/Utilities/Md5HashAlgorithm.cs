﻿using System;
using System.IO;
using System.Security.Cryptography;

namespace UnityBuild.Utilities
{
	public class Md5HashAlgorithm
		: IHashAlgorithm
	{
		#region Public Methods

		public Md5Hash GenerateHash( byte[] data )
		{
			var hash = _md5.ComputeHash( data );
			return new Md5Hash( hash );
		}

		IHash IHashAlgorithm.GenerateHash( byte[] data )
		{
			return GenerateHash( data );
		}

		public Md5Hash GenerateHash( Stream data )
		{
			var hash = _md5.ComputeHash( data );
			return new Md5Hash( hash );
		}

		IHash IHashAlgorithm.GenerateHash( Stream data )
		{
			return GenerateHash( data );
		}

		public HashGenerator CreateGenerator()
		{
			throw new NotImplementedException();
		}

		#endregion

		#region Fields

		private readonly MD5 _md5 = MD5.Create();

		#endregion
	}
}