﻿using System.Text;

namespace UnityBuild.Utilities
{
	public static class StringExtensions
	{
		#region Public Methods

		public static byte[] ToByteArray( this string text )
		{
			return Encoding.Unicode.GetBytes( text );
		}

		#endregion
	}
}