﻿namespace UnityBuild.Utilities
{
	public interface IOwnableBy< T >
	{
		T Owner { get; set; }
	}
}