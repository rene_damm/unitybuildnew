﻿using System;

namespace UnityBuild.Utilities
{
	/// <summary>
	/// An MD5 hash.
	/// </summary>
	public class Md5Hash
		: Hash
	{
		#region Constructors

		public Md5Hash( byte[] rawData )
			: base( rawData )
		{
			if ( rawData.Length != DataLength )
				throw new ArgumentException( "MD5 hashes must be 16 bytes long!", "rawData" );
		}

		#endregion

		#region Public Constants

		/// <summary>
		/// Length of an MD5 hash.
		/// </summary>
		public const int DataLength = 16;

		#endregion
	}
}
