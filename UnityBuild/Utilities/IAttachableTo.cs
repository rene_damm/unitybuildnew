﻿namespace UnityBuild.Utilities
{
	public interface IAttachableTo< THost >
	{
		THost Host { get; }

		void AttachTo( THost host );
	}
}
