﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace UnityBuild.Utilities
{
	public class OptionAttribute : Attribute
	{
		#region Constructors

		public OptionAttribute( params string[] names )
		{
			_names = names.ToList();
		}

		#endregion

		#region Public Properties

		public IReadOnlyList< string > Names
		{
			get { return _names; }
		}

		#endregion

		#region Fields

		private readonly List< string > _names;

		#endregion
	}
}
