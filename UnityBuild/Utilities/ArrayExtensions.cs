﻿using System.IO;
using System.Linq;
using System.Text;

namespace UnityBuild.Utilities
{
	public static class ArrayExtensions
	{
		#region Public Methods

		public static string ToHexString( this byte[] data )
		{
			// Adapted from http://stackoverflow.com/questions/623104/byte-to-hex-string/18574846#18574846.
        
			var count = data.Length;
			var builder = new StringBuilder( count * 2 );
			for ( var i = 0; i < count; ++ i )
			{
				var value = data[ i ];
				builder.Append( _byteToHexStringTable[ value ] );
			}

			return builder.ToString();
		}

		public static MemoryStream ToStream( this byte[] data )
		{
			return new MemoryStream( data );
		}

		#endregion

		#region Fields

		private static readonly string[] _byteToHexStringTable = Enumerable.Range( 0, 256 )
			.Select( v => v.ToString( "x2" ) ).ToArray();

		#endregion
	}
}