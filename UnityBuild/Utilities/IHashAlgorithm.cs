﻿using System.IO;

namespace UnityBuild.Utilities
{
	public interface IHashAlgorithm
	{
		IHash GenerateHash( byte[] data );

		IHash GenerateHash( Stream data );

		HashGenerator CreateGenerator();
	}
}