﻿using System;

namespace UnityBuild.Utilities
{
	public abstract class Completable
		: ICompletable
	{
		#region Public Properties

		public bool IsComplete
		{
			get { return _isComplete; }
			set
			{
				// Make sure we don't go back to being incomplete.
				if ( !value && _isComplete )
					throw new InvalidOperationException( "Cannot set a target back to not being completed once it has been completed!" );

				// Change state.
				_isComplete = value;

				// Raise event.
				if ( _isComplete )
				{
					var onComplete = OnComplete;
					if ( onComplete != null )
						onComplete( this );
				}
			}
		}

		#endregion

		#region Public Events

		public event Action< ICompletable > OnComplete;

		#endregion

		#region Fields

		private bool _isComplete;

		#endregion
	}
}