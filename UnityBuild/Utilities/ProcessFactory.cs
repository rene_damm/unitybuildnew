﻿using System;
using System.Diagnostics;

namespace UnityBuild.Utilities
{
	public class ProcessFactory
		: IProcessFactory
	{
		#region Public Methods

		public IProcess NewProcess( ProcessStartInfo startInfo )
		{
			var process = Process.Start( startInfo );
			if ( process == null )
				throw new NotImplementedException();

			return new ProcessImplementation( process );
		}

		#endregion

		#region Inner Types

		protected class ProcessImplementation
			: IProcess
		{
			#region Constructors

			public ProcessImplementation( Process process )
			{
				if ( process == null )
					throw new ArgumentNullException( "process" );

				Process = process;
			}

			#endregion

			#region Public Properties

			public Process Process { get; private set; }

			#endregion
		}

		#endregion
	}
}