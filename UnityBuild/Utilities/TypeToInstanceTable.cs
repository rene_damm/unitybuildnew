﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace UnityBuild.Utilities
{
	public class TypeToInstanceTable< TBaseType >
		: IReadOnlyDictionary< Type, TBaseType >
		where TBaseType : class
	{
		#region Public Methods

		public void Add( TBaseType item )
		{
			if ( item == null )
				throw new ArgumentNullException( "item" );

			for ( var type = item.GetType(); type != null; type = type.BaseType )
			{
				// Stop if we've hit on a base type that is no
				// longer a TBaseType.
				if ( !typeof( TBaseType ).IsAssignableFrom( type ) )
					break;

				// If we already have a mapping for this type, do not replace it if the existing
				// map has exactly the right type (rather than a base type) and we don't.
				var existingItem = _table.GetValueOrDefault( type );
				if (    existingItem != null
					 && existingItem.GetType() == type
					 && type != item.GetType() )
					continue;

				// Map type to instance.
				_table[ type ] = item;
			}

			// Add it to the list of instances.  No matter how many times we've added it
			// to the table, it only has one mention in this list.
			_instances.Add( item );
		}

		public TBaseType Get( Type type )
		{
			if ( type == null )
				throw new ArgumentNullException( "type" );

			var item = _table.GetValueOrDefault( type );
			if ( item == null )
				throw new KeyNotFoundException( string.Format( "No instance of type '{0}'!", type ) );

			return item;
		}

		public TItem Get< TItem >()
			where TItem : TBaseType
		{
			var item = Get( typeof( TItem ) );
			return ( TItem ) item;
		}

		public bool ContainsKey( Type key )
		{
			return _table.ContainsKey( key );
		}

		public bool TryGetValue( Type key, out TBaseType value )
		{
			return _table.TryGetValue( key, out value );
		}

		public IEnumerator< KeyValuePair< Type, TBaseType > > GetEnumerator()
		{
			return _table.GetEnumerator();
		}

		IEnumerator IEnumerable.GetEnumerator()
		{
			return GetEnumerator();
		}

		#endregion

		#region Public Properties

		public int Count
		{
			get { return _table.Count; }
		}

		public IReadOnlyList< TBaseType > Instances
		{
			get { return _instances; }
		}

		public TBaseType this[ Type key ]
		{
			get { return Get( key ); }
		}

		public IEnumerable< Type > Keys
		{
			get { return _table.Keys; }
		}

		public IEnumerable< TBaseType > Values
		{
			get { return _table.Values; }
		}

		#endregion

		#region Fields

		private readonly List< TBaseType > _instances = new List< TBaseType >();
		private readonly Dictionary< Type, TBaseType > _table = new Dictionary< Type, TBaseType >();

		#endregion
	}
}