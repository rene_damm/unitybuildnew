﻿using System.Diagnostics;

namespace UnityBuild.Utilities
{
	public interface IProcessFactory
	{
		IProcess NewProcess( ProcessStartInfo startInfo );
	}
}