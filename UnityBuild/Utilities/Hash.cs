﻿using System;

namespace UnityBuild.Utilities
{
	public abstract class Hash
		: IHash
	{
		#region Constructors

		protected Hash( byte[] rawData )
		{
			if ( rawData == null )
				throw new ArgumentException( "Hash data cannot be null!", "rawData" );
			if ( rawData.Length == 0 )
				throw new ArgumentException( "Hash data cannot be empty!", "rawData" );

			RawData = rawData;
		}

		#endregion

		#region Public Methods

		public override string ToString()
		{
			return RawData.ToHexString();
		}

		#endregion

		#region Public Properties

		public byte[] RawData { get; private set; }

		#endregion
	}
}