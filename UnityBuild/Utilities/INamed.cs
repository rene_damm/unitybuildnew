﻿namespace UnityBuild.Utilities
{
	public interface INamed
	{
		string Name { get; }
	}
}