﻿using System;
using System.Collections.Generic;

namespace UnityBuild.Utilities
{
    public static class SingletonHelper
    {
        #region Public Methods

        public static T GetSingleton< T >()
            where T : class, new()
        {
            object instance;

            if ( !_allSingletons.TryGetValue( typeof( T ), out instance ) )
            {
                instance = new T();
                _allSingletons[ typeof( T ) ] = instance;
            }

            return ( T ) instance;
        }

        #endregion

        #region Fields

        private static readonly Dictionary< Type, object > _allSingletons = new Dictionary< Type, object >(); 

        #endregion
    }
}
