﻿using System.Collections.Generic;

namespace UnityBuild.Utilities
{
	public static class DictionaryExtensions
	{
		#region Public Methods

		public static TValue GetValueOrDefault< TKey, TValue >( this IDictionary< TKey, TValue > dictionary, TKey key )
		{
			TValue value;
			if ( dictionary.TryGetValue( key, out value ) )
				return value;
			return default( TValue );
		}

		#endregion
	}
}