﻿using UnityBuild.Utilities;

namespace UnityBuild.Artifacts
{
	public interface IArtifactCache
	{
		/// <summary>
		/// Look up an existing artifact in the cache.
		/// </summary>
		/// <param name="hash">Hash of the artifact.</param>
		/// <returns>Artifact instance for the given hash or null if the given artifact
		/// is not in the cache.</returns>
		IArtifact FindArtifact( IHash hash );

		IArtifact CreateArtifact( IHash hash );
	}
}