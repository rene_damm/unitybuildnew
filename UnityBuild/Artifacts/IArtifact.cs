﻿using UnityBuild.Utilities;

namespace UnityBuild.Artifacts
{
	/// <summary>
	/// A blob of data identified by a hash.
	/// </summary>
	public interface IArtifact
		: IHashed
	{
		/// <summary>
		/// Where we currently store the artifact on disk.
		/// </summary>
		string Path { get; }
	}
}