﻿
////REVIEW: return IObservable<OfSomething> that allows monitoring the build?

namespace UnityBuild.Builds
{
	/// <summary>
	/// A build engine figures out which tool executions from the set fed to its through
	/// <see cref="IBuildInputs">build inputs</see>.
	/// </summary>
    public interface IBuildEngine
    {
		/// <summary>
		/// Run all tool executions that need running in the given build.
		/// </summary>
		/// <param name="buildInputs">Aggregated data about what to build.</param>
		void Build( IBuildInputs buildInputs );
    }
}
