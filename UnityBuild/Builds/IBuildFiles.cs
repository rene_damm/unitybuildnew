﻿using UnityBuild.Files;

namespace UnityBuild.Builds
{
	public interface IBuildFiles
	{
		/// <summary>
		/// File system on which the files can actually be found.
		/// </summary>
		IFileSystem FileSystem { get; }

		/// <summary>
		/// Complete set of source files.
		/// </summary>
		IFileCollection Sources { get; }

		/// <summary>
		/// Complete set of intermediate files.
		/// </summary>
		IFileCollection Intermediates { get; }

		/// <summary>
		/// Complete set of output files.
		/// </summary>
		IFileCollection Outputs { get; }		
	}
}