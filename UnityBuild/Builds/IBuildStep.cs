﻿using System.Collections.Generic;
using UnityBuild.Tools;
using UnityBuild.Utilities;

namespace UnityBuild.Builds
{
	/// <summary>
	/// A sequence of mutually independent tool executions that must be completed before
	/// moving on to the next build step.
	/// </summary>
	/// <remarks>
	/// <para>Mutually independent means that none of the tool executions within a single
	/// step may produce output that is consumed as input by another tool execution in
	/// the same step.  Tool executions that feed into each other need to be put into
	/// separate build steps.</para>
	/// 
	/// <para>A good example of such a dependency is the need to have a compile step and
	/// separate link step; individual compiler runs are independent of each other but the
	/// link tool execution is dependent on the compiler runs.</para>
	/// </remarks>
	public interface IBuildStep
		: INamed
		, ICompletable
	{
		/// <summary>
		/// List of tool executions to run for the build phase.
		/// </summary>
		IEnumerable< IToolExecution > ToolExecutions { get; }
	}
}
