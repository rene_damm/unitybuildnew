﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityBuild.Files;

namespace UnityBuild.Builds
{
	public static class BuildFilesExtensions
	{
		#region Public Methods

		public static IEnumerable< IFile > GetSourceFiles( this IBuildFiles files, string path )
		{
			if ( string.IsNullOrEmpty( path ) )
				throw new ArgumentException( "Path cannot be null or empty!", "path" );

			return files.FileSystem.ListFiles( path )
				.Select( matchingPath => files.Sources.GetFile( matchingPath ) );
		}

		#endregion
	}
}