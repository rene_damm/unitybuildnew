﻿using System;
using System.Collections.Generic;

namespace UnityBuild.Builds
{
	public class BuildInputs
		: IBuildInputs
	{
		#region Constructors

		public BuildInputs( IReadOnlyList< IBuildStep > buildSteps, IBuildFiles buildFiles )
		{
			if ( buildSteps == null )
				throw new ArgumentException( "Build steps list cannot be null!", "buildSteps" );
			if ( buildFiles == null )
				throw new ArgumentException( "Build files collection cannot be null!", "buildFiles" );

			BuildSteps = buildSteps;
			BuildFiles = buildFiles;
		}

		#endregion

		#region Public Properties

		public IReadOnlyList< IBuildStep > BuildSteps { get; private set; }

		public IBuildFiles BuildFiles { get; private set; }

		#endregion
	}
}