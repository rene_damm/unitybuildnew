﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityBuild.Artifacts;
using UnityBuild.Files;

////NOTE: build engine shouldn't deal with generating hashes; should be able to assume they're just there
////TODO: once/if we have file collections, do all the update checks in one big bulk operation (probably vectorized)

namespace UnityBuild.Builds
{
	/// <summary>
	/// Build engine implementation.
	/// </summary>
	/// <remarks>
	/// <para>At the moment, this implementation is very naive and inefficient.  It does the minimal amount of work
	/// necessary to deliver the desired behaviors.</para>
	/// </remarks>
	public class BuildEngine
		: IBuildEngine
	{
		#region Constructors

		public BuildEngine( IFileSystem fileSystem, IArtifactCache artifactCache )
		{
			if ( fileSystem == null )
				throw new ArgumentException( "File system cannot be null!", "fileSystem" );

			FileSystem = fileSystem;
			ArtifactCache = artifactCache;
		}

		#endregion

		#region Public Methods

		public void Build( IBuildInputs buildInputs )
		{
			if ( buildInputs == null )
				throw new ArgumentException( "Build inputs cannot be null!", "buildInputs" );

			// Process each step in the build sequence.
			foreach ( var step in buildInputs.BuildSteps )
			{
                // Process each tool execution in the step.
				foreach ( var toolExecution in step.ToolExecutions )
				{
					var tool = toolExecution.Tool;
					var toolInputs = toolExecution.ToolInputs;
					var inputFiles = toolInputs.InputFiles;
					var outputFiles = toolInputs.OutputFiles;

                    // Get most recent modification time of any of the input
                    // files.
                    var lastInputFileWriteTime = GetLastWriteTime( inputFiles );
                    if ( lastInputFileWriteTime == DateTime.MinValue &&
                         toolInputs.InputFiles.Any() )
                    {
                        ////TODO: decide what to do if inputs are missing
                        throw new NotImplementedException();
                    }

                    // Get most recent modification time of any of the output
                    // files.  Will be DateTime.MinValue if one or more of the outputs
                    // don't exist.
                    var lastOutputFileWriteTime = GetLastWriteTime( outputFiles );

                    // Outputs are stable if output modification times are more recent.
				    var stable = lastInputFileWriteTime < lastOutputFileWriteTime;
				    toolInputs.Stable = stable;

                    // If outputs are not stable, we may need to run the tool.
                    if ( !stable )
					{
						////REVIEW: how does the tool inputs itself factor into this?  is the hash on the output file enough?  doesn't it need to tool inputs as a feed?

						// But first try to gather all the necessary artifacts from the
						// cache.
						var artifacts = GetCachedArtifacts( outputFiles );
						if ( artifacts != null )
						{
							// All outputs are found in the cache.
							FileSystem.CopyFiles
								(
									  artifacts.Select( x => x.Path )
									, outputFiles.Select( x => x.Path )
								);
						}
						else
						{
							////TODO: for intermediate files, we should directly generate them into the artifact cache

							// One or more of the outputs are not available, so
							// run the tool.
							tool.Run( toolInputs );

							// Put our outputs in the cache.
							StoreInCache( outputFiles );
						}
					}

					// Done with tool execution.
					toolExecution.IsComplete = true;
				}

				// Done with phase.
				step.IsComplete = true;
			}
		}

		#endregion

        #region Non-Public Methods

		/// <summary>
		/// Get most recent modification time of any of the files in the list (and their includes).
		/// </summary>
		/// <param name="files">List of files.</param>
		/// <returns>Most recent modification time or <see cref="DateTime.MinValue"/> .</returns>
        protected DateTime GetLastWriteTime( IEnumerable< IFile > files )
        {
            var lastWriteTime = DateTime.MinValue;

            foreach ( var file in files )
            {
				////OPTIMIZE: for files that are produced from other tool executions, it is very inefficient to later check them the same way
				////	as source files; we already know they have changed

				// Grab file path.
				var path = file.Path;
				if ( string.IsNullOrEmpty( path ) )
					throw new InvalidOperationException(
						string.Format( "Generated file '{0}' has not been assigned a path!", file ) );

				// Grab file system info for file.
				var fileInfo = FileSystem.GetFileInfo( path );
                if ( fileInfo == null )
                    return DateTime.MinValue;

				// Grab last write time.
                var fileWriteTime = fileInfo.LastWriteTime;
                if ( lastWriteTime < fileWriteTime )
                    lastWriteTime = fileWriteTime;

				////OPTIMIZE: this is very inefficient; we may be checking the same files over and over again

				// Check include files.
				var includeWriteTime = GetLastWriteTime( file.Includes );
				if ( lastWriteTime < includeWriteTime )
					lastWriteTime = includeWriteTime;
            }

            return lastWriteTime;
        }

		/// <summary>
		/// Translate the given set of files to a set of cached artifacts.
		/// </summary>
		/// <param name="files">Set of files.</param>
		/// <returns>A complete set of artifacts for the given files or null if one or more
		/// artifacts could not be found in the cache.</returns>
		protected IEnumerable< IArtifact > GetCachedArtifacts( IEnumerable< IFile > files )
		{
			if ( ArtifactCache == null )
				return null;

			var artifacts = new List< IArtifact >();

			////OPTIMIZE: this is very inefficient as we may look up artifacts twice

			foreach ( var file in files )
			{
				var artifact = ArtifactCache.FindArtifact( file.Hash );
				if ( artifact == null )
					return null;

				artifacts.Add( artifact );
			}

			if ( artifacts.Count == 0 )
				return null;

			return artifacts;
		}

		/// <summary>
		/// Put all files not marked as <see cref="IFile.DontCache"/> in the artifact cache.
		/// </summary>
		/// <param name="files">List of files.</param>
		protected void StoreInCache( IEnumerable< IFile > files )
		{
			foreach ( var file in files )
			{
				if ( file.DontCache )
					continue;

				var existingArtifact = ArtifactCache.FindArtifact( file.Hash );
				if ( existingArtifact != null )
					continue;

				var newArtifact = ArtifactCache.CreateArtifact( file.Hash );
				////FIXME: this assumes the artifact path is relative to the file system root
				FileSystem.CopyFile( file.Path, newArtifact.Path );
			}
		}

        #endregion

        #region Public Properties

        public IFileSystem FileSystem { get; private set; }

		public IArtifactCache ArtifactCache { get; private set; }

		#endregion
	}
}