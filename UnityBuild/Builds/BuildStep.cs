﻿using System;
using System.Collections.Generic;
using UnityBuild.Tools;
using UnityBuild.Utilities;

namespace UnityBuild.Builds
{
	public class BuildStep
		: Completable
		, IBuildStep
	{
		#region Constructors

		public BuildStep( string name )
		{
			if ( string.IsNullOrEmpty( name ) )
				throw new ArgumentException( "Name cannot be null or empty!", "name" );

			Name = name;
		}

		#endregion

		#region Public Properties

		public string Name { get; private set; }

		/// <summary>
		/// List of tool executions to run for this build step.
		/// </summary>
		public List< IToolExecution > ToolExecutions
        {
            get { return _toolExecutions; }
        }
		
		IEnumerable< IToolExecution > IBuildStep.ToolExecutions
		{
			get { return ToolExecutions; }
		}

		#endregion

		#region Fields

		private readonly List< IToolExecution > _toolExecutions = new List< IToolExecution >();

		#endregion
	}
}