﻿using System.Collections.Generic;

namespace UnityBuild.Builds
{
	/// <summary>
	/// Aggregation of data that we feed into a build.
	/// </summary>
	public interface IBuildInputs
	{
		/// <summary>
		/// Ordered list of build steps we have to execute.
		/// </summary>
		IReadOnlyList< IBuildStep > BuildSteps { get; }
		
		IBuildFiles BuildFiles { get; }
	}
}
