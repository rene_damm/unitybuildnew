﻿using System;
using UnityBuild.Files;

namespace UnityBuild.Builds
{
	public class BuildFiles
		: IBuildFiles
	{
		#region Constructors

		public BuildFiles( IFileSystem fileSystem, IFileCollection sources, IFileCollection intermediates, IFileCollection outputs )
		{
			if ( fileSystem == null )
				throw new ArgumentException( "File system cannot be null!", "fileSystem" );
			if ( sources == null )
				throw new ArgumentException( "Source file collection cannot be null!", "sources" );
			if ( intermediates == null )
				throw new ArgumentException( "Intermediate file collection cannot be null!", "intermediates" );
			if ( outputs == null )
				throw new ArgumentException( "Output file collection cannot be null!", "outputs" );

			FileSystem = fileSystem;
			Sources = sources;
			Intermediates = intermediates;
			Outputs = outputs;
		}

		#endregion

		#region Public Properties

		public IFileSystem FileSystem { get; private set; }
		public IFileCollection Sources { get; private set; }
		public IFileCollection Intermediates { get; private set; }
		public IFileCollection Outputs { get; private set; }

		#endregion
	}
}