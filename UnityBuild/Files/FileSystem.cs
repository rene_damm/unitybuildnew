﻿using System;
using System.Collections.Generic;
using System.IO;

namespace UnityBuild.Files
{
	public class FileSystem
		: IFileSystem
	{
		#region Constructors

		public FileSystem( string rootPath )
		{
			RootPath = rootPath ?? string.Empty;
			RootPath = RootPath.Replace( '/', '\\' );
			if (    RootPath != string.Empty 
				 && RootPath[ RootPath.Length - 1 ] != '\\' )
				RootPath += '\\';
		}

		#endregion

		#region Public Methods

		public IFileInfo GetFileInfo( string path )
		{
			var fullPath = GetFullPath( path );
			return new FileInfoWrapper( new FileInfo( fullPath ) );
		}

		public Stream OpenFile( string path )
		{
			throw new NotImplementedException();
		}

		public Stream CreateFile( string path )
		{
			throw new NotImplementedException();
		}

		public void CreateDirectory( string path )
		{
			throw new NotImplementedException();
		}

		public IEnumerable< string > ListFiles( string path )
		{
			var fullPath = GetFullPath( path );

			////TODO: support search paths

			// Get attributes of file at path.
			FileAttributes attributes;
			try
			{
				attributes = System.IO.File.GetAttributes( fullPath );
			}
			catch ( FileNotFoundException )
			{
				yield break;
			}

			if ( ( attributes & FileAttributes.Directory ) == FileAttributes.Directory )
			{
				foreach ( var entry in Directory.GetFiles( fullPath ) )
					yield return GetRelativePath( entry );
			}
			else
			{
				yield return GetRelativePath( fullPath );
			}
		}

		public IEnumerable< string > ListDirectories( string path )
		{
			throw new NotImplementedException();
		}

		public void CopyFile( string fromPath, string toPath )
		{
			throw new NotImplementedException();
		}

		public void MoveFile( string fromPath, string toPath )
		{
			throw new NotImplementedException();
		}

		public void DeleteFile( string path )
		{
			throw new NotImplementedException();
		}

		#endregion

		#region Non-Public Methods

		protected string GetFullPath( string path )
		{
			if ( string.IsNullOrEmpty( path ) )
				throw new ArgumentException( "Path cannot be null or empty!", "path" );

			path = path.Replace( '/', '\\' );
			return Path.Combine( RootPath, path );
		}

		protected string GetRelativePath( string fullPath )
		{
			if ( string.IsNullOrEmpty( fullPath ) )
				throw new ArgumentException( "Full path cannot be null or empty!", "fullPath" );

			if ( !fullPath.StartsWith( RootPath ) )
				throw new ArgumentException( string.Format( "Path '{0}' is not on file system rooted at '{1}'!", fullPath, RootPath ) );

			var path = fullPath.Substring( RootPath.Length );

			return path.Replace( '\\', '/' );
		}

		#endregion

		#region Public Properties

		public string RootPath { get; private set; }

		#endregion

		#region Inner Types

		protected class FileInfoWrapper
			: IFileInfo
		{
			public FileInfoWrapper( FileInfo fileInfo )
			{
			}

			#region Public Properties

			public string Path
			{
				get { throw new NotImplementedException(); }
			}

			public DateTime LastWriteTime
			{
				get { throw new NotImplementedException(); }
			}

			#endregion
		}

		#endregion
	}
}