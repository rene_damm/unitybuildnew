﻿using System.Collections.Generic;
using UnityBuild.Utilities;

namespace UnityBuild.Files
{
	public class File
		: IFile
	{
		#region Public Properties

		public IFileCollection Owner { get; set; }
		public IHash Hash { get; set; }
		public string Path { get; private set; }

		public string FullPath
		{
			get
			{
				if ( _fullPath == null && Owner != null )
					return System.IO.Path.Combine( Owner.RootPath, Path );

				return _fullPath;
			}
		}
	
		public IEnumerable< IFile > Includes { get; private set; }
		public IFileState FileState { get; set; }
		public bool DontCache { get; private set; }

		#endregion

		#region Fields

		private string _fullPath;

		#endregion
	}
}