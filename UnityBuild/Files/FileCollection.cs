﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityBuild.Utilities;

////connect to file state cache (optional)
////connect to artifact cache (optional)  NO!  build engine should be the only thing connecting to the artifact cache

namespace UnityBuild.Files
{
	/// <summary>
	/// A collection of files that can connect to a persistent cache of file states ...
	/// </summary>
	public class FileCollection
		: IFileCollection
	{
		#region Constructors

		public FileCollection(
			  string rootPath
			, Func< IFile > fileConstructor
			, Func< string, IFileStateCache > fileStateCacheConstructor )
		{
			if ( fileConstructor == null )
				throw new ArgumentNullException( "fileConstructor" );
			//if ( fileStateCacheConstructor == null )
			//	throw new ArgumentNullException( "fileStateCacheConstructor" );
			if ( rootPath == null )
				rootPath = string.Empty;

			RootPath = rootPath;
			_fileConstructor = fileConstructor;
		}

		#endregion

		#region Public Methods

		public IFile GetFile( string path = null )
		{
			var havePath = !string.IsNullOrEmpty( path );

			// See if we have an existing file at that path.
			if ( havePath )
			{
				var existing = _paths.GetValueOrDefault( path );
				if ( existing != null )
				{
					// Yes, so simply return it.
					return existing;
				}
			}

			// Construct new file.
			var file = _fileConstructor();
			_files.Add( file );

			// Own it.
			file.Owner = this;

			// Add to path map.
			if ( havePath )
				_paths[ path ] = file;

			return file;
		}

		public IEnumerator< IFile > GetEnumerator()
		{
			return _files.GetEnumerator();
		}

		IEnumerator IEnumerable.GetEnumerator()
		{
			return GetEnumerator();
		}

		#endregion

		#region Public Properties

		public string RootPath { get; private set; }

		public int Count
		{
			get { return _files.Count; }
		}

		public IReadOnlyDictionary< string, IFile > Paths
		{
			get { return _paths; }
		}

		#endregion

		#region Fields

		private readonly Func< IFile > _fileConstructor;
		private readonly Dictionary< string, IFile > _paths = new Dictionary< string, IFile >();
		private readonly List< IFile > _files = new List< IFile >();

		#endregion
	}
}