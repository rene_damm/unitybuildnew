﻿using System.Collections.Generic;
using UnityBuild.Artifacts;
using UnityBuild.Utilities;

namespace UnityBuild.Files
{
	/// <summary>
	/// A path-based blob of data that may change over time.
	/// </summary>
	public interface IFile
		: IArtifact
		//, IHashable ////this doesn't seem like a good way to expose something that may be expensive to compute first
		, IOwnableBy< IFileCollection >
	{
		//IMimeType MimeType { get; }


		bool DontCache { get; }


		////REVIEW: this seems to expose internals
		IFileState FileState { get; set; }

		////REVIEW: this seems misplaced
		IEnumerable< IFile > Includes { get; }
	}
}