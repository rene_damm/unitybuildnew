﻿using System;

namespace UnityBuild.Files
{
    public interface IFileInfo
    {
		////REVIEW: really have this on here as well?  kinda odd since IFile has the path too

		string Path { get; }

        DateTime LastWriteTime { get; }
    }
}