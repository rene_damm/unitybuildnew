﻿using System.Collections.Generic;
using System.IO;

namespace UnityBuild.Files
{
	public interface IFileSystem
	{
		////TODO: refactor to query for file infos in batches
		IFileInfo GetFileInfo( string path );

		Stream OpenFile( string path );

		Stream CreateFile( string path );

		void CreateDirectory( string path );

		IEnumerable< string > ListFiles( string path );

		IEnumerable< string > ListDirectories( string path );

		void CopyFile( string fromPath, string toPath );

		void MoveFile( string fromPath, string toPath );

		void DeleteFile( string path );
	}
}
