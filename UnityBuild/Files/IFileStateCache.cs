﻿namespace UnityBuild.Files
{
	public interface IFileStateCache
	{
		IFileState[] GetOrAddFileStates( params string[] paths );
	}
}
