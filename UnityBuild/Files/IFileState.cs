﻿using System;
using System.Collections.Generic;
using UnityBuild.Utilities;

////REVIEW: this makes sense only for sources

namespace UnityBuild.Files
{
	/// <summary>
	/// Information about a file that we keep between builds.
	/// </summary>
	public interface IFileState
		: IHashed
	{
		string Path { get; }

		Hash LastHash { get; set; }

		Hash LastContentHash { get; set; }

		DateTime LastBuildTime { get; set; }

		IList< IHashed > HashInputs { get; }

		//IList< xxx > HashOutputs { get; } 
	}
}
