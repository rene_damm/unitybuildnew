﻿using System;
using System.Collections.Generic;

namespace UnityBuild.Files
{
	public static class FileSystemExtensions
	{
		#region Public Methods

		public static void CopyFiles( this IFileSystem fileSystem, IEnumerable< string > fromPaths, IEnumerable< string > toPaths )
		{
			var fromPathEnumerator = fromPaths.GetEnumerator();
			var toPathEnumerator = toPaths.GetEnumerator();

			while ( true )
			{
				var fromPathsHasNext = fromPathEnumerator.MoveNext();
				var toPathsHasNext = toPathEnumerator.MoveNext();

				if ( !fromPathsHasNext || !toPathsHasNext )
				{
					if ( fromPathsHasNext != toPathsHasNext )
						throw new ArgumentException( "Number of elements in 'fromPaths' and 'toPaths' does not match!", "fromPaths" );

					break;
				}

				var from = fromPathEnumerator.Current;
				var to = toPathEnumerator.Current;

				fileSystem.CopyFile( from, to );
			}
		}

		#endregion
	}
}