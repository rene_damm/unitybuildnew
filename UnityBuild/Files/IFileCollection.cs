﻿using System.Collections.Generic;

////thoughts:
//// - if we try to add the same source file more than once, our setup must be screwy; however, i fear that is just the reality with the unity build
////    really?  what about building the same target in two different configs?
//// - the identity of source files is easy to establish as is the identity of output files; intermediate files are hard
////	for them, we might need to allow separate IFiles to end up referring to the same intermediate
//// - might also enforce that the path on every file must be set right away but then we need hashes to be available immediately when
////    constructing paths for intermediates; also it may be harder to synchronize this with the artifact cache which ultimately should
////    have the authority in deciding where the intermediates will be stored

namespace UnityBuild.Files
{
	////decision 1: keep file state cache or not?
	////decision 2: 

	/// <summary>
	/// A collection of files that reside under a common root path.
	/// </summary>
	public interface IFileCollection
		: IReadOnlyCollection< IFile >
	{
		/// <summary>
		/// Root path under which all the files in the collection reside.
		/// </summary>
		/// <remarks>May be an empty string in which case the collection is rooted
		/// at the file system root.</remarks>
		string RootPath { get; }

		IReadOnlyDictionary< string, IFile > Paths { get; }

		////?? how do we handle things when a path is assigned later on?
			
		IFile GetFile( string path = null );
	}
}
