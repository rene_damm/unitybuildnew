﻿using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using Autofac;
using UnityBuild.Artifacts;
using UnityBuild.Builds;
using UnityBuild.Files;
using UnityBuild.Targets;
using UnityBuild.Tools;
using UnityBuild.Utilities;
using File = UnityBuild.Files.File;

namespace UnityBuild
{
	public class UnityBuildRunner
	{
		#region Constructors

		public UnityBuildRunner( string workingCopyPath = null )
		{
			if ( string.IsNullOrEmpty( workingCopyPath ) )
				workingCopyPath = InferWorkingCopyPath();

			WorkingCopyPath = workingCopyPath;
		}

		#endregion

		#region Public Methods

		public int Run( params string[] args )
		{
			if ( args == null || args.Length < 1 )
				throw new ArgumentException( "Must have at least one argument!", "args" );

			// Set up container.
			var builder = new ContainerBuilder();
			RegisterTypes( builder );
			var container = builder.Build();

			// Grab arguments.
			var targetName = args[ 0 ];
			var buildArgs = args.Skip( 1 ).ToArray();

			// Run build system.
			var buildSystem = container.Resolve< IBuildSystem >();
			buildSystem.RunBuild( targetName, buildArgs );

			return 0;
		}

		#endregion

		#region Non-Public Methods

		protected virtual void RegisterTypes( ContainerBuilder builder )
		{
			builder.RegisterType< BuildSystem >()
				.As< IBuildSystem >()
				.SingleInstance();

			builder.RegisterType< BuildEngine >()
				.As< IBuildEngine >()
				.SingleInstance();

            builder.RegisterType< TargetCollection >()
				.As< ITargetCollection >()
				.InstancePerDependency();

			builder.RegisterType< TargetSettings >()
				.As< ITargetSettings >()
				.InstancePerDependency();

			builder.RegisterType< ToolChain >()
				.As< IToolChain >()
				.SingleInstance();

			builder.RegisterType< FileCollection >()
				.As< IFileCollection >()
				.InstancePerDependency();

			builder.RegisterType< File >()
				.As< IFile >()
				.InstancePerDependency();

			builder.RegisterType< ArtifactCache >()
				.As< IArtifactCache >()
				.InstancePerDependency();

			builder.RegisterType< Md5HashAlgorithm >()
				.As< IHashAlgorithm >()
				.SingleInstance();

			builder.RegisterType< ProcessFactory >()
				.As< IProcessFactory >()
				.SingleInstance();

			// Set up file system.
			builder.RegisterType< FileSystem >()
				.As< IFileSystem >()
				.WithParameter( "rootPath", WorkingCopyPath )
				.SingleInstance();

			////TODO: this just makes the assumption that target names correspond to their type names; this logic shouldn't be built into here

			// Register all our targets.
			builder.RegisterAssemblyTypes( Assembly.GetExecutingAssembly() )
				.Where( type => !type.IsAbstract && type.IsClass && typeof( ITarget ).IsAssignableFrom( type ) )
				.Named< ITarget >( type => type.Name )
				.InstancePerDependency();

			// Register all our tools.
			builder.RegisterAssemblyTypes( Assembly.GetExecutingAssembly() )
				.Where( type => !type.IsAbstract && type.IsClass && typeof( ITool ).IsAssignableFrom( type ) )
				.As< ITool >()
				.SingleInstance();

			// Register all our tool settings.
			builder.RegisterAssemblyTypes( Assembly.GetExecutingAssembly() )
				.Where( type => !type.IsAbstract && type.IsClass && typeof( IToolSettings ).IsAssignableFrom( type ) )
				.As< IToolSettings >()
				.SingleInstance();
		}

		private static string InferWorkingCopyPath()
		{
			////TODO: properly detect working copy path
			return @"D:\Unity\trunk-clean";

			// By default, we assume that we are located at the root of the Unity source tree.
			return Path.GetDirectoryName( Process.GetCurrentProcess().MainModule.FileName );
		}

		#endregion

		#region Public Properties

		public string WorkingCopyPath { get; private set; }

		#endregion
	}
}