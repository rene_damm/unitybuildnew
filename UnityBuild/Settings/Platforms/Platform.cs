﻿using System;
using System.Collections.Generic;
using UnityBuild.Settings.Platforms.Desktops;
using UnityBuild.Utilities;

namespace UnityBuild.Settings.Platforms
{
    public abstract class Platform
    {
        #region Public Methods

        public static Platform GetDefault()
        {
            switch ( Environment.OSVersion.Platform )
            {
                case PlatformID.MacOSX:
                    return GetPlatform< MacPlatform >();

                case PlatformID.Unix:
                    return GetPlatform< LinuxPlatform >();

                default:
                    return GetPlatform< WindowsPlatform >();
            }
        }

        public static TPlatform GetPlatform< TPlatform >()
            where TPlatform : Platform, new()
        {
            return SingletonHelper.GetSingleton< TPlatform >();
        }

        #endregion

        #region Public Properties

        public static IEnumerable< Platform > AllPlatforms { get; private set; }

        #endregion
    }
}
