﻿using System;
using UnityBuild.Settings.Architectures;
using UnityBuild.Settings.Platforms;
using UnityBuild.Utilities;

namespace UnityBuild.Settings
{
    public abstract class BuildSettings
        : IBuildSettings
    {
	    #region Public Methods

	    public object Clone()
	    {
		    throw new NotImplementedException();
	    }

	    #endregion

	    #region Public Properties

		[ Option ]
	    public Architecture Architecture { get; set; }

		[ Option ]
	    public Platform Platform { get; set; }

		[ Option( "internal", "developer" ) ]
	    public bool IsInternalBuild { get; set; }

	    [ Option( "debug" ) ]
	    public bool IsDebugBuild { get; set; }

		[ Option( "optimized" ) ]
	    public bool IsOptimizedBuild { get; set; }

	    [ Option( "release" ) ]
	    public bool IsReleaseBuild
	    {
		    get { return !IsDebugBuild && !IsInternalBuild && IsOptimizedBuild; }
			set
			{
				IsDebugBuild = false;
				IsInternalBuild = false;
				IsOptimizedBuild = true;
			}
	    }

		[ Option( "nodeps", "nodependencies" ) ]
	    public bool DontBuildDependencies { get; private set; }

		#endregion
    }
}