﻿using System;
using UnityBuild.Settings.Architectures;
using UnityBuild.Settings.Platforms;

////REVIEW: assumes that a single platform and a single architecture is used
////TODO: make equatable

namespace UnityBuild.Settings
{
    /// <summary>
    /// 
    /// </summary>
    /// <remarks>
    /// <para>A single set of settings only supports a single architecture and a
    /// single platform selection based on the assumption that a single tool execution
    /// cannot yield output for multiple platforms and/or architectures at once ...</para>
    /// </remarks>
    public interface IBuildSettings
        : ICloneable
    {
		/// <summary>
		/// Architecture to build for.
		/// </summary>
		Architecture Architecture { get; }

        /// <summary>
        /// Platform to build for.
        /// </summary>
		Platform Platform { get; }

        /// <summary>
        /// Whether the build is for internal use.
        /// </summary>
        bool IsInternalBuild { get; }

        /// <summary>
        /// Whether the build should include debugging information.
        /// </summary>
        bool IsDebugBuild { get; }

        /// <summary>
        /// Whether generated code should be optimized.
        /// </summary>
        bool IsOptimizedBuild { get; }

		/// <summary>
		/// Whether to automatically build dependencies or not.
		/// </summary>
		bool DontBuildDependencies { get; }
    }
}
