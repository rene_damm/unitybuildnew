﻿using UnityBuild.Targets.Abstract;

namespace UnityBuild.Targets
{
	public abstract class Editor
		: CompileAndLinkTarget
	{
		#region Non-Public Methods

		protected override void SetUp()
		{
			base.SetUp();

			// Add components.
			AddComponent< UnityRuntime >();
		}

		#endregion
	}
}