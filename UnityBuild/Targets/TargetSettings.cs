﻿using System;
using UnityBuild.Settings;
using UnityBuild.Tools;

namespace UnityBuild.Targets
{
	public class TargetSettings
		: BuildSettings
		, ITargetSettings
	{
		#region Constructors

		public TargetSettings( IToolChain toolChain )
		{
			if ( toolChain == null )
				throw new ArgumentNullException( "toolChain" );

			ToolChain = toolChain;
		}

		#endregion

		#region Public Properties

		public IToolChain ToolChain { get; private set; }

		#endregion
	}
}
