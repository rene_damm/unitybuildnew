﻿using System;

namespace UnityBuild.Targets
{
    public class TargetDependency
        : ITargetDependency
    {
        #region Public Properties

        public Type TargetType { get; private set; }
        public ITargetSettings TargetSettings { get; private set; }

        #endregion
    }
}