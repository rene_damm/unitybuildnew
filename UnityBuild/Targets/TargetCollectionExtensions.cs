﻿namespace UnityBuild.Targets
{
	/// <summary>
	/// Extension methods for <see cref="ITargetCollection"/>.
	/// </summary>
	public static class TargetCollectionExtensions
	{
		#region Public Methods

		public static TTarget FindOrAddTargetByType< TTarget >( this ITargetCollection targetCollection, ITargetSettings settings )
		{
			return ( TTarget ) targetCollection.FindOrAddTargetByType( typeof( TTarget ), settings );
		}

		#endregion
	}
}