﻿using UnityBuild.Settings;
using UnityBuild.Tools;

////REVIEW: hashing doesn't necessarily make sense for this

namespace UnityBuild.Targets
{
    public interface ITargetSettings
        : IBuildSettings
    {
        IToolChain ToolChain { get; }
    }
}
