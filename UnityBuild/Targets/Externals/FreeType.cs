﻿using UnityBuild.Targets.Abstract;

namespace UnityBuild.Targets.Externals
{
	public class FreeType
		: TargetComponentFor< CompileAndLinkTarget >
	{
		#region Non-Public Methods

		protected override void SetUp()
		{
			Target.Compile(
				  "External/freetype2/src/base/ftbitmap.c"
				, "External/freetype2/src/base/ftcalc.c"
				, "External/freetype2/src/base/ftgloadr.c"
				, "External/freetype2/src/base/ftglyph.c"
				, "External/freetype2/src/base/ftinit.c"
				, "External/freetype2/src/base/ftobjs.c"
				, "External/freetype2/src/base/ftoutln.c"
				, "External/freetype2/src/base/ftrfork.c"
				, "External/freetype2/src/base/ftstream.c"
				, "External/freetype2/src/base/ftstroke.c"
				, "External/freetype2/src/base/ftsystem.c"
				, "External/freetype2/src/base/fttrigon.c"
				, "External/freetype2/src/base/ftutil.c"

				, "External/freetype2/src/cff/cffdrivr.c"
				, "External/freetype2/src/cff/cffgload.c"
				, "External/freetype2/src/cff/cffload.c"
				, "External/freetype2/src/cff/cffobjs.c"
				, "External/freetype2/src/cff/cffparse.c"
				, "External/freetype2/src/cff/cffcmap.c"

				, "External/freetype2/src/psnames/psmodule.c"
				, "External/freetype2/src/psnames/pspic.c"

				, "External/freetype2/src/sfnt/sfobjs.c"
				, "External/freetype2/src/sfnt/sfdriver.c"
				, "External/freetype2/src/sfnt/ttcmap.c"
				, "External/freetype2/src/sfnt/ttmtx.c"
				, "External/freetype2/src/sfnt/ttpost.c"
				, "External/freetype2/src/sfnt/ttload.c"
				, "External/freetype2/src/sfnt/ttsbit.c"
				, "External/freetype2/src/sfnt/ttkern.c"

				, "External/freetype2/src/smooth/ftgrays.c"
				, "External/freetype2/src/smooth/ftsmooth.c"

				, "External/freetype2/src/truetype/ttdriver.c"
				, "External/freetype2/src/truetype/ttobjs.c"
				, "External/freetype2/src/truetype/ttpload.c"
				, "External/freetype2/src/truetype/ttgload.c"
				, "External/freetype2/src/truetype/ttgxvar.c"
				, "External/freetype2/src/truetype/ttinterp.c"

				, "External/freetype2/src/pshinter/pshrec.c"
				, "External/freetype2/src/pshinter/pshglob.c"
				, "External/freetype2/src/pshinter/pshalgo.c"
				, "External/freetype2/src/pshinter/pshmod.c"
				, "External/freetype2/src/pshinter/pshpic.c"

				, "External/freetype2/src/raster/ftraster.c"
				, "External/freetype2/src/raster/ftrend1.c"
				, "External/freetype2/src/raster/rastpic.c"
			);
		}

		#endregion
	}
}