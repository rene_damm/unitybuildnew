﻿using UnityBuild.Targets.Abstract;
using UnityBuild.Targets.Externals;

namespace UnityBuild.Targets
{
	public class UnityRuntime
		: TargetComponentFor< CompileAndLinkTarget >
	{
		#region Non-Public Methods

		protected override void SetUp()
		{
			Target.AddComponent< FreeType >();
			Target.AddComponent< ShaderLab >();
		}

		#endregion
	}
}