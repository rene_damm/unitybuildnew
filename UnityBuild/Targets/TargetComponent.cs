﻿using System;
using UnityBuild.Builds;
using UnityBuild.Utilities;

namespace UnityBuild.Targets
{
	/// <summary>
	/// A piece of build logic that can be plugged into other targets and contributes
	/// build configuration data to them.  This allows factoring out individual
	/// bits of build logic independent of the target class hierarchy.
	/// </summary>
	public abstract class TargetComponent
		: IAttachableTo< Target >
	{
		#region Public Methods

		public virtual void AttachTo( Target target )
		{
			if ( target == null )
				throw new ArgumentException( "Target cannot be null!", "target" );
			if ( Target != null )
				throw new InvalidOperationException( "Cannot attach to target; already attached to a target!" );

			Target = target;

			SetUp();
		}

		#endregion

		#region Non-Public Methods

		protected abstract void SetUp();

		#endregion

		#region Public Properties

		public Target Target { get; private set; }

		Target IAttachableTo< Target >.Host
		{
			get { return Target; }
		}

		public ITargetSettings Settings
		{
			get { return Target.Settings; }
		}

		public IBuildFiles BuildFiles
		{
			get { return Target.BuildFiles; }
		}

		#endregion
	}
}