﻿using System.Collections.Generic;
using UnityBuild.Builds;
using UnityBuild.Utilities;

namespace UnityBuild.Targets
{
	/// <summary>
	/// A named collection of build steps.
	/// </summary>
	public interface ITarget
		: INamed
		, ICompletable
	{
		/// <summary>
		/// Settings that this target uses.
		/// </summary>
		/// <remarks>Only valid after calling <see cref="Initialize"/>.</remarks>
		ITargetSettings Settings { get; }

		/// <summary>
        /// List of targets that must have their builds completed before this target
        /// is built.
        /// </summary>
        /// <remarks>
        /// <para>This doesn't reference targets directly as we may want to keep data
        /// from dependencies out of a build &#8211; if, for example, we do not want
        /// to build dependencies or if we want to isolate the data for a specific
        /// target.</para>
        /// </remarks>
        IEnumerable< ITargetDependency > Dependencies { get; }

        /// <summary>
        /// Build steps that must be executed to build the target.
        /// </summary>
		/// <remarks>Only valid after calling <see cref="Initialize"/>.</remarks>
		IEnumerable< IBuildStep > BuildSteps { get; }

		/// <summary>
		/// Have the target set up its build.
		/// </summary>
		/// <param name="name">Name that the target should use.</param>
		/// <param name="settings">Settings supplied to the target.  Note that the target
		/// is free to actually configure itself using different settings which means that
		/// <see cref="Settings"/> may end up not being the <see cref="settings"/> instance
		/// we pass in here.</param>
		/// <param name="buildFiles">Access to the collection of files used by the build.
		/// This is where the target should add its own files.</param>
		void Initialize( string name, ITargetSettings settings, IBuildFiles buildFiles );
	}
}
