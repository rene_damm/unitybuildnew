﻿using System;
using UnityBuild.Builds;
using UnityBuild.Tools;
using UnityBuild.Tools.Cpp;

namespace UnityBuild.Targets.Abstract
{
	/// <summary>
	/// A two-phased target that first compiles a set of source files and then
	/// links them all together.
	/// </summary>
	/// <remarks>
	/// <para>There's some assumptions made by this class:</para>
	/// <list type="bullet">
	///		<item>
	///			<description>There may be several types of input files but there is only one type of object
	///				file produced by a compile run.</description>
	///		</item>
	/// </list>
	/// </remarks>
	public abstract class CompileAndLinkTarget
		: Target
	{
		#region Non-Public Methods

		protected override void SetUp()
		{
			base.SetUp();

			// Add phases.
			CompileStep = AddBuildStep( "Compile" );
			LinkStep = AddBuildStep( "Link" );

			//
			if ( _compiler == null )
				Compiler = Settings.ToolChain.GetTool< CppCompiler >();
			if ( _compilerSettings == null )
				CompilerSettings = Settings.ToolChain.GetToolSettings< CppCompilerSettings >();
			if ( _linker == null )
				Linker = Settings.ToolChain.GetTool< CppLinker >();
			if ( _linkerSettings == null )
				LinkerSettings = Settings.ToolChain.GetToolSettings< CppLinkerSettings >();
		}

		#endregion

		#region Public Methods

		public void Compile( params string[] paths )
		{
			foreach ( var path in paths )
			{
				var sourceFiles = BuildFiles.GetSourceFiles( path );
				foreach ( var sourceFile in sourceFiles )
				{
					var toolExecution = new ToolExecution( Compiler, CompilerSettings );
					toolExecution.AddInputFile( sourceFile );

					var objectFile = BuildFiles.Intermediates.GetFile();
					toolExecution.AddOutputFile( objectFile );

					CompileStep.ToolExecutions.Add( toolExecution );
				}
			}
		}

		#endregion

		#region Public Properties

		public BuildStep CompileStep { get; private set; }

		public BuildStep LinkStep { get; private set; }

		public ITool Compiler
		{
			get { return _compiler; }
			set
			{
				if ( value == null )
					throw new ArgumentException( "Compiler cannot be null!", "value" );

				_compiler = value;
			}
		}

		public ITool Linker
		{
			get { return _linker; }
			set
			{
				if ( value == null )
					throw new ArgumentException( "Linker cannot be null!", "value" );

				_linker = value;
			}
		}

		public IToolSettings CompilerSettings
		{
			get { return _compilerSettings; }
			set
			{
				if ( value == null )
					throw new ArgumentException( "Compiler settings cannot be null!", "value" );

				_compilerSettings = value;
			}
		}

		public IToolSettings LinkerSettings
		{
			get { return _linkerSettings; }
			set
			{
				if ( value == null )
					throw new ArgumentException( "Linker settings cannot be null!", "value" );

				_linkerSettings = value;
			}
		}

		#endregion

		#region Fields

		private ITool _compiler;
		private ITool _linker;
		private IToolSettings _compilerSettings;
		private IToolSettings _linkerSettings;

		#endregion
	}
}
