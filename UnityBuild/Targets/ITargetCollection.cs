﻿using System;
using System.Collections.Generic;
using UnityBuild.Builds;

namespace UnityBuild.Targets
{
	/// <summary>
	/// A set of targets.
	/// </summary>
	public interface ITargetCollection
		: IReadOnlyCollection< ITarget >
	{
        /// <summary>
        /// Collective build files for all targets in the collection.
        /// </summary>
        IBuildFiles BuildFiles { get; }

		ITarget FindOrAddTargetByName( string targetName, ITargetSettings settings );

		ITarget FindOrAddTargetByType( Type type, ITargetSettings settings );
	}
}
