﻿using System;

namespace UnityBuild.Targets
{
    public interface ITargetDependency
    {
        Type TargetType { get; }

        ITargetSettings TargetSettings { get; }
    }
}