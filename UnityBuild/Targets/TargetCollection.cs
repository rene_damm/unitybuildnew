﻿using System;
using System.Collections;
using System.Collections.Generic;
using Autofac.Features.Indexed;
using UnityBuild.Builds;

namespace UnityBuild.Targets
{
	using TargetConstructor = Func< ITarget >;

	public class TargetCollection
        : ITargetCollection
	{
		#region Constructors

	    public TargetCollection( IBuildFiles buildFiles, IIndex< string, TargetConstructor > targetConstructors )
	    {
		    if ( buildFiles == null )
				throw new ArgumentException( "Build inputs cannot be null!", "buildFiles" );
			if ( targetConstructors == null )
				throw new ArgumentException( "Target constructor index cannot be null!", "targetConstructors" );

			BuildFiles = buildFiles;
			_targetConstructors = targetConstructors;
	    }

		#endregion

		#region Public Methods

        public ITarget FindOrAddTargetByName( string targetName, ITargetSettings settings )
        {
			if ( string.IsNullOrEmpty( targetName ) )
				throw new ArgumentException( "Target name cannot be null or empty!", "targetName" );
			if ( settings == null )
				throw new ArgumentException( "Target settings cannot be null!", "settings" );

			// See if we already have the target in the list.
			foreach ( var existingTarget in _targets )
			{
				if ( existingTarget.Name == targetName && existingTarget.Settings == settings )
					return existingTarget;
			}

			// Look up constructor.
			TargetConstructor constructor;
			if ( !_targetConstructors.TryGetValue( targetName, out constructor ) )
				throw new KeyNotFoundException( string.Format( "No target named '{0}' has been registered!", targetName ) );

			// Create target.
			var newTarget = constructor();
			newTarget.Initialize( targetName, settings, BuildFiles );

			// Add to list;
			_targets.Add( newTarget );

			return newTarget;
        }

        public ITarget FindOrAddTargetByType( Type type, ITargetSettings settings )
        {
            throw new NotImplementedException();
        }

		public IEnumerator< ITarget > GetEnumerator()
        {
            return _targets.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

		#endregion

		#region Public Properties

		public IBuildFiles BuildFiles { get; private set; }

	    public int Count
	    {
		    get { return _targets.Count; }
	    }

		#endregion

		#region Fields

		private readonly List< ITarget > _targets = new List< ITarget >();
		private readonly IIndex< string, TargetConstructor > _targetConstructors;

		#endregion
    }
}