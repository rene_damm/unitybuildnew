﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using UnityBuild.Builds;
using UnityBuild.Settings.Architectures;
using UnityBuild.Settings.Platforms;
using UnityBuild.Utilities;

////TODO: allow target to specify which platforms and architectures are valid for it

namespace UnityBuild.Targets
{
	public abstract class Target
		: Completable
		, ITarget
	{
		#region Public Methods

		public void Initialize( string name, ITargetSettings settings, IBuildFiles buildFiles )
		{
			if ( string.IsNullOrEmpty( name ) )
				throw new ArgumentException( "Target name cannot be null or empty!", "name" );
			if ( settings == null )
				throw new ArgumentException( "Target settings cannot be null!", "settings" );
			if ( buildFiles == null )
				throw new ArgumentException( "Build files cannot be null!", "buildFiles" );

			if ( _isInitialized )
				throw new InvalidOperationException( "Target has already been initialized!" );
			_isInitialized = true;

			Name = name;
			Settings = settings;
			BuildFiles = buildFiles;

			SetUp();
		}

        public BuildStep AddBuildStep( string name )
        {
			// Create new step.
            var buildStep = new BuildStep( name );
			buildStep.OnComplete += _ => CompleteIfAllBuildStepsHaveCompleted();

			// Add to list.
			_buildSteps.Add( buildStep );
            
			return buildStep;
        }

		public void AddComponent< TPart >()
			where TPart : TargetComponent, new()
		{
			// Ignore if we already have an instance of this part in the list.  Don't respect
			// subtyping here but rather check for an exact type match.
			if ( _parts.Any( x => x.GetType() == typeof( TPart ) ) )
				return;

			// Create and initialize part.
			var part = new TPart();
			part.AttachTo( this );

			// Add to list.
			_parts.Add( part );
		}

		public void AddDependency< TTarget >( ITargetSettings overrideSettings = null )
			where TTarget : ITarget
		{
			throw new NotImplementedException();
		}

		#endregion

		#region Non-Public Methods

		protected virtual void SetUp()
        {
			Debug.Assert( Settings != null, "Settings not set!" );
			Debug.Assert( BuildFiles != null, "BuildFiles not set!" );
        }

		private void CompleteIfAllBuildStepsHaveCompleted()
		{
			if ( BuildSteps.All( x => x.IsComplete ) )
				IsComplete = true;
		}

		#endregion

		#region Public Properties

		public string Name { get; private set; }

		public ITargetSettings Settings { get; private set; }

        public IBuildFiles BuildFiles { get; private set; }

		public List< BuildStep > BuildSteps
		{
			get { return _buildSteps; }
		}

		IEnumerable< IBuildStep > ITarget.BuildSteps
		{
			get { return BuildSteps; }
		}
					
		public IEnumerable< TargetDependency > Dependencies
		{
			get { return _dependencies; }
		}

        IEnumerable< ITargetDependency > ITarget.Dependencies
        {
            get { return Dependencies; }
        }

		public IEnumerable< TargetComponent > Parts
		{
			get { return _parts; }
		}

        public Architecture DefaultArchitecture { get; set; }

        public Platform DefaultPlatform { get; set; }

        public IEnumerable< Architecture > CompatibleArchitectures { get; set; }
 
        public IEnumerable< Platform > CompatiblePlatforms { get; set; }

        #endregion

		#region Fields

		private bool _isInitialized;
		private readonly List< TargetDependency > _dependencies = new List< TargetDependency >();
		private readonly List< BuildStep > _buildSteps = new List< BuildStep >();
		private readonly List< TargetComponent > _parts = new List< TargetComponent >();

		#endregion
	}
}