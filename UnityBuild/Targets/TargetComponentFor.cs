﻿using System;

namespace UnityBuild.Targets
{
	/// <summary>
	/// A target component that restricts to what type of target it can be added.
	/// </summary>
	/// <typeparam name="TTarget">Type of target the component can be added to.</typeparam>
	public abstract class TargetComponentFor< TTarget >
		: TargetComponent
		where TTarget : Target
	{
		#region Public Methods

		public sealed override void AttachTo( Target target )
		{
			var targetImpl = target as TTarget;
			if ( target != null && targetImpl == null )
				throw new InvalidOperationException(
					string.Format (
						  "Cannot add part '{0}' to targets of type '{1}'!  Expecting targets of type '{2}'!"
						, this
						, target.GetType()
						, typeof( TTarget ) )
				);

			Target = targetImpl;
			base.AttachTo( target );
		}

		#endregion

		#region Public Properties

		public new TTarget Target { get; private set; }

		#endregion
	}
}
