﻿namespace UnityBuild
{
	/// <summary>
	/// The top-level front-end to the build system.
	/// </summary>
	public interface IBuildSystem
	{
		void RunBuild( string targetName, params string[] args );
	}
}