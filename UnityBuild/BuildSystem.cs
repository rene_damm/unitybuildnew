﻿using System;
using System.Collections.Generic;
using UnityBuild.Builds;
using UnityBuild.Files;
using UnityBuild.Targets;
using UnityBuild.Utilities;

////takes care of synthesizing build settings
////takes care of getting the target instance and configuring it
////takes care of running the build engine on that instance with the given setup

////TODO: we should have a default "build all" target to fall back to

namespace UnityBuild
{
	public class BuildSystem
		: IBuildSystem
	{
		#region Constructors

		public BuildSystem(
			  IBuildEngine buildEngine
			, IFileSystem fileSystem
			, Func< string, IFileCollection > createFileCollection
			, Func< IBuildFiles, ITargetCollection > createTargetCollection
            , Func< ITargetSettings > createTargetSettings )
		{
			if ( buildEngine == null )
				throw new ArgumentException( "Build engine cannot be null!", "buildEngine" );
			if ( fileSystem == null )
				throw new ArgumentException( "File system cannot be null!", "fileSystem" );
			if ( createFileCollection == null )
				throw new ArgumentException( "File collection constructor cannot be null!", "createFileCollection" );
			if ( createTargetCollection == null )
				throw new ArgumentException( "Target collection constructor cannot be null!", "createTargetCollection" );
			if ( createTargetSettings == null )
				throw new ArgumentException( "Target settings constructor cannot be null!", "createTargetSettings" );

			_buildEngine = buildEngine;
			_fileSystem = fileSystem;
			_createFileCollection = createFileCollection;
			_createTargetCollection = createTargetCollection;
            _createTargetSettings = createTargetSettings;
		}

		#endregion

		#region Public Methods

		public void RunBuild( string targetName, params string[] args )
		{
			if ( string.IsNullOrEmpty( targetName ) )
				throw new ArgumentException( "Target name cannot be null or empty!", "targetName" );

            // Set up settings for target.
            var settings = _createTargetSettings();

			// Configure target settings by parsing arguments.
			OptionParser.ParseOptions( settings, args );

			// Create our collections to hold the files we process
			// during building.
			var sources = _createFileCollection( "" );
			var intermediates = _createFileCollection( "artifacts" );
			var outputs = _createFileCollection( "build" );
			var allFiles = new BuildFiles( _fileSystem, sources, intermediates, outputs );

			// Create object in which we store all the data to set up
			// our target with.
			var allTargets = _createTargetCollection( allFiles );

			// Create and set up target.
			var target = allTargets.FindOrAddTargetByName( targetName, settings );

			// Set up a list of targets in the order we need to build them.
			var orderedTargetList = new List< ITarget >();
			if ( !settings.DontBuildDependencies )
				CollectDependencies( target, settings, allTargets, orderedTargetList );
			orderedTargetList.Add( target );

			// Gather build steps.
			var buildSteps = CollectBuildSteps( orderedTargetList );

			// Create build inputs.
			var buildInputs = new BuildInputs( buildSteps, allFiles );

			// Build.
			_buildEngine.Build( buildInputs );
		}

		#endregion

		#region Non-Public Methods

		protected void CollectDependencies( ITarget target, ITargetSettings targetSettings, ITargetCollection targetCollection, List< ITarget > orderedTargetList )
		{
			foreach ( var dependency in target.Dependencies )
			{
				var dependencyTargetType = dependency.TargetType;
				var dependencyTargetSettings = dependency.TargetSettings;

				if ( dependencyTargetSettings == null )
					dependencyTargetSettings = targetSettings;

				if ( dependencyTargetSettings.DontBuildDependencies )
					continue;

				var resultingTarget = targetCollection
					.FindOrAddTargetByType( dependencyTargetType, dependencyTargetSettings );

				if ( orderedTargetList.Contains( resultingTarget ) )
					continue;

				CollectDependencies( resultingTarget, dependencyTargetSettings, targetCollection, orderedTargetList );

				orderedTargetList.Add( resultingTarget );
			}
		}

		protected static List< IBuildStep > CollectBuildSteps( List< ITarget > orderedTargetList )
		{
			var buildSteps = new List< IBuildStep >();

			foreach ( var target in orderedTargetList )
				buildSteps.AddRange( target.BuildSteps );

			return buildSteps;
		}

		#endregion

		#region Fields

		private readonly IBuildEngine _buildEngine;
		private readonly IFileSystem _fileSystem;
		private readonly Func< string, IFileCollection > _createFileCollection;
		private readonly Func< IBuildFiles, ITargetCollection > _createTargetCollection;
	    private readonly Func< ITargetSettings > _createTargetSettings;

	    #endregion
	}
}